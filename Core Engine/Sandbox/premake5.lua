-- Game Premake
project "Sandbox"
    kind "ConsoleApp"
    language "C++"

    local targetDirectory = "%{sln.location}\\bin"
    local objectDirectory = "%{sln.location}\\bin-int\\"

    targetdir(targetDirectory)
    objdir(objectDirectory)
    debugdir("%{sln.location}")

    dependson {
        "GLFW",
        "Engine"
    }

    files {
        "include/**.h",
        "src/**.cpp"
    }

    includedirs {
        "include",
        "%{sln.location}\\Engine\\include",
        "%{IncludeDir.catch}",
        "%{IncludeDir.GLFW}",
        "%{IncludeDir.glm}",
        "%{IncludeDir.json}",
        "%{IncludeDir.spdlog}",
        "%{IncludeDir.stb}",
        "%{IncludeDir.diligent}",
        "%{IncludeDir.diligentspirv}",
        "%{IncludeDir.tbb}",
        "%{IncludeDir.gltf}"
    }

    libdirs {
		targetDirectory,
        "%{LibDir.diligent}",
        "%{LibDir.diligentspirv}",
        "%{LibDir.tbb}"
    }

    bindirs {
        "%{BinDir.tbb}"
    }

    links {
        "Engine",
        "GLFW",
        "DiligentCore",
        "glslangd",
        "HLSLd",
        "OGLCompilerd",
        "OSDependentd",
        "spirv-cross-core",
        "SPIRVd",
        "SPIRV-Tools-opt",
        "SPIRV-Tools",
        "glew-static",
        "vulkan-1",
        "dxgi",
        "d3d11",
        "d3d12",
        "d3dcompiler",
        "opengl32",
        "tbb",
        "tbbmalloc",
        "tbbmalloc_proxy"
    }

    defines {
    	"GLFW_INCLUDE_NONE",
        "GLM_FORCE_DEPTH_ZERO_TO_ONE",
        "PLATFORM_WIN32=1",
        "D3D11_SUPPORTED=1",
        "D3D12_SUPPORTED=1",
        "GL_SUPPORTED=1"
	}

	disablewarnings {
		"4005"
    }
    
    filter "system:windows"
        cppdialect "C++17"
        systemversion "latest"
        staticruntime "Off"

        ignoredefaultlibraries {
            "LIBCMT",
            "LIBCMTD"
        }

        linkoptions {
            "/ignore:4006",
            "/ignore:4221",
            "/ignore:4099",
            "/ignore:4075"
        }

        defines {
            "CORE_PLATFORM_WINDOWS"
        }

    filter "system:linux"
        staticruntime "Off"
    
        buildoptions {
            "-std=c++17",
            "-fPIC"
        }

        defines {
            "CORE_PLATFORM_LINUX"
        }

        libdirs {
            "/usr/lib/x86_64-linux-gnu"
        }

    filter "configurations:Debug"
        defines { 
            "CORE_DEBUG",
            "CORE_ENABLE_ASSERTS"
        }

        staticruntime "Off"
        runtime "Debug"

        symbols "On"

    filter "configurations:Release"
        defines { 
            "CORE_RELEASE",
			"NDEBUG"
        }

        staticruntime "Off"
        runtime "Release"

        optimize "On"

    filter "configurations:Dist"
        defines {
            "CORE_DIST",
			"NDEBUG"
        }

        staticruntime "Off"
        runtime "Release"

        optimize "On"