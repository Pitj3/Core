#include <Core.h>

int main()
{
	Application* app = new Application(EGraphicsContextType::Directx11);

	const Timer t;
	const bool fileExists = FileSystem::instance().exists("Core.sln");
	const double elapsed = t.elapsedMillis();

	CORE_TRACE("File exists: {0}", fileExists);
	CORE_TRACE("Elapsed: {0}", elapsed);

	File* file = new File();
	file->write("Changing file shit bruv");
	file->save("TestFile.txt", EFileSaveMode::Truncate);

	File* loadFile = FileSystem::instance().load("Core.sln");
	CORE_TRACE("Core.sln file loaded: {0}", loadFile->data());

	CORE_TRACE("TestFile.txt file size: {0}", FileSystem::instance().getSize("TestFile.txt"));

	app->run();

	delete app;
	delete file;
	delete loadFile;

	return 0;
}