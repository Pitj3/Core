#include "Components/StaticMeshRenderer.h"

#include "Graphics/GraphicsTypes.h"
#include "Graphics/GraphicsContext.h"

StaticMeshRenderer::StaticMeshRenderer(StaticMesh* aMesh)
{
	mMesh = aMesh;
}

void StaticMeshRenderer::onConstruct()
{

}

void StaticMeshRenderer::onRender()
{
	mMesh->mVertexBuffer->bind();
	mMesh->mIndexBuffer->bind();

	DrawAttributes drawAttribs;
	drawAttribs.indexType = EValueType::VT_UINT32;
	drawAttribs.isIndexed = true;
	drawAttribs.numIndices = static_cast<uint32_t>(mMesh->indices.get().size());
	drawAttribs.flags = EDrawFlags::FLAG_VERIFY_ALL;

	GraphicsContext::instance().draw(drawAttribs);
}
