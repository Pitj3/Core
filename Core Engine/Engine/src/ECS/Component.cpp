#include "ECS/Component.h"
#include "ECS/Entity.h"
#include "ECS/EntityManager.h"

Component::~Component()
{
	entity->_removeComponent(this);
	EntityManager::instance().mComponentPool->freeBlock(static_cast<void*>(this));
}
