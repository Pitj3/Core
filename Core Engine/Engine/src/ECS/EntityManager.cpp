#include "ECS/EntityManager.h"
#include "ECS/Entity.h"

#include "Core/Log.h"

#include "Components/TransformComponent.h"

EntityManager& EntityManager::instance()
{
	static EntityManager* instance = new EntityManager();
	return *instance;
}

Entity* EntityManager::create(const std::string& aName)
{
	if(mEntityMap.size() > 0)
	{
		if(mEntityMap.find(aName) != mEntityMap.end())
		{
			CORE_INTERNAL_ERROR("Entity already exists with name {0}", aName);
			return nullptr;
		}
	}

	Entity* entity = static_cast<Entity*>(mEntityPool->getBlock());
	::new(entity) Entity(aName);

	entity->mManager = this;
	entity->transform = entity->addComponent<TransformComponent>();

	return entity;
}

Entity* EntityManager::get(const std::string& aName)
{
	if (mEntityMap.size() > 0)
	{
		const auto iter = mEntityMap.find(aName);
		if (iter != mEntityMap.end())
		{
			return iter->second;
		}
	}

	return nullptr;
}

void EntityManager::destroy(Entity* aEntity)
{
	if (!aEntity)
		return;

	const auto iter = mEntityMap.find(aEntity->name());
	if(iter != mEntityMap.end())
	{
		mEntityMap.erase(iter);

		mEntityPool->freeBlock(static_cast<void*>(aEntity));
		aEntity = nullptr;
	}
}

void EntityManager::destroyAll() const
{
	delete mEntityPool;
	delete mComponentPool;
}

void EntityManager::setEventCallback(const EventCallbackFunction& aCallback)
{
	mCallback = aCallback;
}

EntityManager::EntityManager()
{
	mEntityPool = new PoolAllocator(64, 1000000, 512);
	mComponentPool = new PoolAllocator(64, 1000000, 512);
}
