#include "Graphics/StaticMesh.h"

struct Vertex
{
	Vector3f position;
	Vector2f uv;
	Vector3f normal;
	Vector3f tangent;
	Vector3f binormal;
};

StaticMesh::StaticMesh()
{
	mVertexBuffer = nullptr;
	mIndexBuffer = nullptr;
}

StaticMesh::~StaticMesh()
{
	delete mVertexBuffer;
	delete mIndexBuffer;
}

void StaticMesh::build()
{
	const size_t vertexCount = positions.get().size();
	Vertex* vList = new Vertex[vertexCount];
	for(size_t i = 0; i < vertexCount; i++)
	{
		vList[i].position = positions.get()[i];
		if (uvs.get().size() > i)
			vList[i].uv = uvs.get()[i];
		else
			vList[i].uv = Vector2f(0, 0);
		if (normals.get().size() > i)
			vList[i].normal = normals.get()[i];
		else
			vList[i].normal = Vector3f(0, 1, 0);
		if (tangents.get().size() > i)
			vList[i].tangent = tangents.get()[i];
		else
			vList[i].tangent = Vector3f(0, 0, 0);
		if (binormals.get().size() > i)
			vList[i].binormal = binormals.get()[i];
		else
			vList[i].binormal = Vector3f(0, 0, 0);
	}

	const size_t indexCount = indices.get().size();
	uint32_t* iList = new uint32_t[indexCount];
	for(size_t i = 0; i < indexCount; i++)
	{
		iList[i] = indices.get()[i];
	}

	mVertexBuffer = new VertexBuffer(vList, sizeof(Vertex) * vertexCount);
	mIndexBuffer = new IndexBuffer(iList, sizeof(uint32_t) * indexCount);
}

void StaticMesh::recalculateBounds()
{
	
}

void StaticMesh::recalculateNormals()
{
	
}

void StaticMesh::recalculateBinormals()
{
	for (size_t i = 0; i < tangents.get().size(); i++)
	{
		const float w = binormals[i].x;
		binormals[i] = normals[i].cross(tangents[i]) * w;
	}
}