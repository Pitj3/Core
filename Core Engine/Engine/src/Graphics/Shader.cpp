#include "Graphics/Shader.h"

#include <Common/interface/RefCntAutoPtr.h>
#include <Graphics/GraphicsEngine/interface/Shader.h>
#include "Graphics/GraphicsContext.h"
#include <Graphics/GraphicsEngine/interface/RenderDevice.h>

class ShaderImpl
{
	public:
		Diligent::RefCntAutoPtr<Diligent::IShader> shader;

		~ShaderImpl()
		{

		}
};

Shader::Shader(const std::string& aName, const std::string& aSource, const EShaderType aType, const EShaderLanguage aLanguage)
{
	mImpl = new ShaderImpl();

	Diligent::ShaderCreateInfo shaderCI;
	shaderCI.SourceLanguage = static_cast<Diligent::SHADER_SOURCE_LANGUAGE>(aLanguage);
	shaderCI.UseCombinedTextureSamplers = true;

	shaderCI.Desc.ShaderType = static_cast<Diligent::SHADER_TYPE>(aType);
	shaderCI.EntryPoint = "main";
	shaderCI.Desc.Name = aName.c_str();
	shaderCI.Source = aSource.c_str();

	Diligent::IRenderDevice* device = static_cast<Diligent::IRenderDevice*>(GraphicsContext::instance().getGraphicsDevice());
	device->CreateShader(shaderCI, &mImpl->shader);
}

Shader::~Shader()
{
	delete mImpl;
}

void* Shader::getProgram() const
{
	return &*mImpl->shader;
}
