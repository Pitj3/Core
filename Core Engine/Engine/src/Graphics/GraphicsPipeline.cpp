#include "Graphics/GraphicsPipeline.h"

#include "Graphics/GraphicsEngineD3D11/interface/EngineFactoryD3D11.h"
#include "Graphics/GraphicsEngineD3D12/interface/EngineFactoryD3D12.h"
#include "Graphics/GraphicsEngineOpenGL/interface/EngineFactoryOpenGL.h"

#include "Common/interface/RefCntAutoPtr.h"
#include "Graphics/GraphicsContext.h"


class GraphicsPipelineImpl
{
	public:
		Diligent::RefCntAutoPtr<Diligent::IPipelineState> pipeline;
		Diligent::RefCntAutoPtr<Diligent::IShaderResourceBinding> shaderBinding;

		bool hasMaterial = false;

		~GraphicsPipelineImpl()
		{

		}
};

GraphicsPipeline::GraphicsPipeline(const std::string& aName, const GraphicsPipelineStateInfo& aCreateInfo)
{
	mImpl = new GraphicsPipelineImpl();

	Diligent::PipelineStateDesc psoDesc;
	psoDesc.IsComputePipeline = false;
	psoDesc.Name = aName.c_str();

	if(aCreateInfo.vs != nullptr)
		psoDesc.GraphicsPipeline.pVS = static_cast<Diligent::IShader*>(aCreateInfo.vs->getProgram());

	if(aCreateInfo.ps != nullptr)
		psoDesc.GraphicsPipeline.pPS = static_cast<Diligent::IShader*>(aCreateInfo.ps->getProgram());

	if (aCreateInfo.ds != nullptr)
		psoDesc.GraphicsPipeline.pDS = static_cast<Diligent::IShader*>(aCreateInfo.ds->getProgram());

	if (aCreateInfo.hs != nullptr)
		psoDesc.GraphicsPipeline.pHS = static_cast<Diligent::IShader*>(aCreateInfo.hs->getProgram());

	if (aCreateInfo.gs != nullptr)
		psoDesc.GraphicsPipeline.pGS = static_cast<Diligent::IShader*>(aCreateInfo.gs->getProgram());

	psoDesc.GraphicsPipeline.BlendDesc.AlphaToCoverageEnable = aCreateInfo.blendDesc.alphaToCoverageEnable;
	psoDesc.GraphicsPipeline.BlendDesc.IndependentBlendEnable = aCreateInfo.blendDesc.independentBlendEnable;
	for (int i = 0; i < aCreateInfo.blendDesc.maxRenderTargets; i++)
	{
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].BlendEnable = aCreateInfo.blendDesc.renderTargets[i].blendEnable;
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].BlendOp = static_cast<Diligent::BLEND_OPERATION>(aCreateInfo.blendDesc.renderTargets[i].blendOp);
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].BlendOpAlpha = static_cast<Diligent::BLEND_OPERATION>(aCreateInfo.blendDesc.renderTargets[i].blendOpAlpha);
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].DestBlend = static_cast<Diligent::BLEND_FACTOR>(aCreateInfo.blendDesc.renderTargets[i].dstBlend);
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].SrcBlend = static_cast<Diligent::BLEND_FACTOR>(aCreateInfo.blendDesc.renderTargets[i].srcBlend);
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].SrcBlendAlpha = static_cast<Diligent::BLEND_FACTOR>(aCreateInfo.blendDesc.renderTargets[i].srcBlendAlpha);
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].DestBlendAlpha = static_cast<Diligent::BLEND_FACTOR>(aCreateInfo.blendDesc.renderTargets[i].dstBlendAlpha);
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].LogicOp = static_cast<Diligent::LOGIC_OPERATION>(aCreateInfo.blendDesc.renderTargets[i].logicOp);
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].LogicOperationEnable = aCreateInfo.blendDesc.renderTargets[i].logicOperationEnable;
		psoDesc.GraphicsPipeline.BlendDesc.RenderTargets[i].RenderTargetWriteMask = static_cast<uint8_t>(aCreateInfo.blendDesc.renderTargets[i].renderTargetWriteMask);
	}

	psoDesc.GraphicsPipeline.SampleMask = aCreateInfo.sampleMask;
	psoDesc.GraphicsPipeline.RasterizerDesc.AntialiasedLineEnable = aCreateInfo.rasterizerState.antialiasedEnable;
	psoDesc.GraphicsPipeline.RasterizerDesc.CullMode = static_cast<Diligent::CULL_MODE>(aCreateInfo.rasterizerState.cullMode);
	psoDesc.GraphicsPipeline.RasterizerDesc.DepthBias = aCreateInfo.rasterizerState.depthBias;
	psoDesc.GraphicsPipeline.RasterizerDesc.DepthBiasClamp = aCreateInfo.rasterizerState.depthBiasClamp;
	psoDesc.GraphicsPipeline.RasterizerDesc.DepthClipEnable = aCreateInfo.rasterizerState.depthClipEnable;
	psoDesc.GraphicsPipeline.RasterizerDesc.SlopeScaledDepthBias = aCreateInfo.rasterizerState.slopeScaledDepthBias;
	psoDesc.GraphicsPipeline.RasterizerDesc.FillMode = static_cast<Diligent::FILL_MODE>(aCreateInfo.rasterizerState.fillMode);
	psoDesc.GraphicsPipeline.RasterizerDesc.ScissorEnable = aCreateInfo.rasterizerState.scissorEnable;
	psoDesc.GraphicsPipeline.RasterizerDesc.FrontCounterClockwise = aCreateInfo.rasterizerState.frontCounterClockwise;

	psoDesc.GraphicsPipeline.DepthStencilDesc.DepthEnable = aCreateInfo.depthStencilState.depthEnable;
	psoDesc.GraphicsPipeline.DepthStencilDesc.DepthFunc = static_cast<Diligent::COMPARISON_FUNCTION>(aCreateInfo.depthStencilState.depthFunc);
	psoDesc.GraphicsPipeline.DepthStencilDesc.DepthWriteEnable = aCreateInfo.depthStencilState.depthWriteEnable;
	psoDesc.GraphicsPipeline.DepthStencilDesc.StencilEnable = aCreateInfo.depthStencilState.stencilEnable;
	psoDesc.GraphicsPipeline.DepthStencilDesc.StencilReadMask = aCreateInfo.depthStencilState.stencilReadMask;
	psoDesc.GraphicsPipeline.DepthStencilDesc.StencilWriteMask = aCreateInfo.depthStencilState.stencilWriteMask;
	
	psoDesc.GraphicsPipeline.DepthStencilDesc.FrontFace.StencilDepthFailOp = static_cast<Diligent::STENCIL_OP>(aCreateInfo.depthStencilState.frontFace.stencilDepthFailOp);
	psoDesc.GraphicsPipeline.DepthStencilDesc.FrontFace.StencilFailOp = static_cast<Diligent::STENCIL_OP>(aCreateInfo.depthStencilState.frontFace.stencilFailOp);
	psoDesc.GraphicsPipeline.DepthStencilDesc.FrontFace.StencilPassOp = static_cast<Diligent::STENCIL_OP>(aCreateInfo.depthStencilState.frontFace.stencilPassOp);
	psoDesc.GraphicsPipeline.DepthStencilDesc.FrontFace.StencilFunc = static_cast<Diligent::COMPARISON_FUNCTION>(aCreateInfo.depthStencilState.frontFace.stencilFunc);

	psoDesc.GraphicsPipeline.DepthStencilDesc.BackFace.StencilDepthFailOp = static_cast<Diligent::STENCIL_OP>(aCreateInfo.depthStencilState.backFace.stencilDepthFailOp);
	psoDesc.GraphicsPipeline.DepthStencilDesc.BackFace.StencilFailOp = static_cast<Diligent::STENCIL_OP>(aCreateInfo.depthStencilState.backFace.stencilFailOp);
	psoDesc.GraphicsPipeline.DepthStencilDesc.BackFace.StencilPassOp = static_cast<Diligent::STENCIL_OP>(aCreateInfo.depthStencilState.backFace.stencilPassOp);
	psoDesc.GraphicsPipeline.DepthStencilDesc.BackFace.StencilFunc = static_cast<Diligent::COMPARISON_FUNCTION>(aCreateInfo.depthStencilState.backFace.stencilFunc);

	Diligent::LayoutElement* elements = new Diligent::LayoutElement[aCreateInfo.inputLayout.numElements];
	for(int i = 0; i < static_cast<int>(aCreateInfo.inputLayout.numElements); i++)
	{
		LayoutElementInfo iii = aCreateInfo.inputLayout.layoutElements[i];
		elements[i] = Diligent::LayoutElement(aCreateInfo.inputLayout.layoutElements[i].inputIndex,
			aCreateInfo.inputLayout.layoutElements[i].bufferSlot, aCreateInfo.inputLayout.layoutElements[i].numComponents,
			static_cast<Diligent::VALUE_TYPE>(aCreateInfo.inputLayout.layoutElements[i].valueType), aCreateInfo.inputLayout.layoutElements[i].isNormalized,
			aCreateInfo.inputLayout.layoutElements[i].relativeOffset, aCreateInfo.inputLayout.layoutElements[i].stride,
			static_cast<Diligent::LayoutElement::FREQUENCY>(aCreateInfo.inputLayout.layoutElements[i].frequency), aCreateInfo.inputLayout.layoutElements[i].instanceDataStepRate);
	}

	psoDesc.GraphicsPipeline.InputLayout.LayoutElements = elements;
	psoDesc.GraphicsPipeline.InputLayout.NumElements = aCreateInfo.inputLayout.numElements;

	psoDesc.GraphicsPipeline.PrimitiveTopology = static_cast<Diligent::PRIMITIVE_TOPOLOGY>(aCreateInfo.primitiveTopology);

	psoDesc.GraphicsPipeline.NumViewports = aCreateInfo.numViewports;
	psoDesc.GraphicsPipeline.NumRenderTargets = aCreateInfo.numRenderTargets;

	for(int i = 0; i < 8; i++)
	{
		psoDesc.GraphicsPipeline.RTVFormats[i] = static_cast<Diligent::TEXTURE_FORMAT>(aCreateInfo.RTVFormats[i]);
	}

	psoDesc.GraphicsPipeline.DSVFormat = static_cast<Diligent::TEXTURE_FORMAT>(aCreateInfo.DSVFormat);

	psoDesc.GraphicsPipeline.SmplDesc.Count = aCreateInfo.sampleInfo.count;
	psoDesc.GraphicsPipeline.SmplDesc.Quality = aCreateInfo.sampleInfo.quality;

	psoDesc.GraphicsPipeline.NodeMask = aCreateInfo.nodeMask;

	psoDesc.ResourceLayout.DefaultVariableType = static_cast<Diligent::SHADER_RESOURCE_VARIABLE_TYPE>(aCreateInfo.resourceLayout.defaultVariableType);
	psoDesc.ResourceLayout.NumStaticSamplers = aCreateInfo.resourceLayout.numStaticSamplers;
	psoDesc.ResourceLayout.NumVariables = aCreateInfo.resourceLayout.numVariables;
	Diligent::StaticSamplerDesc* descs = new Diligent::StaticSamplerDesc[psoDesc.ResourceLayout.NumStaticSamplers];
	for(uint32_t i = 0; i < psoDesc.ResourceLayout.NumStaticSamplers; i++)
	{
		descs[i].Desc.AddressU = static_cast<const Diligent::TEXTURE_ADDRESS_MODE>(aCreateInfo.resourceLayout.staticSamplers[i].desc.addressU);
		descs[i].Desc.AddressV = static_cast<const Diligent::TEXTURE_ADDRESS_MODE>(aCreateInfo.resourceLayout.staticSamplers[i].desc.addressV);
		descs[i].Desc.AddressW = static_cast<const Diligent::TEXTURE_ADDRESS_MODE>(aCreateInfo.resourceLayout.staticSamplers[i].desc.addressW);
		descs[i].Desc.BorderColor[0] = aCreateInfo.resourceLayout.staticSamplers[i].desc.borderColor[0];
		descs[i].Desc.BorderColor[1] = aCreateInfo.resourceLayout.staticSamplers[i].desc.borderColor[1];
		descs[i].Desc.BorderColor[2] = aCreateInfo.resourceLayout.staticSamplers[i].desc.borderColor[2];
		descs[i].Desc.BorderColor[3] = aCreateInfo.resourceLayout.staticSamplers[i].desc.borderColor[3];
		descs[i].Desc.ComparisonFunc = static_cast<Diligent::COMPARISON_FUNCTION>(aCreateInfo.resourceLayout.staticSamplers[i].desc.comparisonFunc);
		descs[i].Desc.MagFilter = static_cast<Diligent::FILTER_TYPE>(aCreateInfo.resourceLayout.staticSamplers[i].desc.magFilter);
		descs[i].Desc.MinFilter = static_cast<Diligent::FILTER_TYPE>(aCreateInfo.resourceLayout.staticSamplers[i].desc.minFilter);
		descs[i].Desc.MinLOD = aCreateInfo.resourceLayout.staticSamplers[i].desc.minLOD;
		descs[i].Desc.MaxLOD = aCreateInfo.resourceLayout.staticSamplers[i].desc.maxLOD;
		descs[i].Desc.MaxAnisotropy = aCreateInfo.resourceLayout.staticSamplers[i].desc.maxAnisotropy;
		descs[i].Desc.MipFilter = static_cast<Diligent::FILTER_TYPE>(aCreateInfo.resourceLayout.staticSamplers[i].desc.mipFilter);
		descs[i].Desc.MipLODBias = aCreateInfo.resourceLayout.staticSamplers[i].desc.mipLODBias;
		descs[i].SamplerOrTextureName = aCreateInfo.resourceLayout.staticSamplers[i].samplerOrTextureName;
		descs[i].ShaderStages = static_cast<Diligent::SHADER_TYPE>(aCreateInfo.resourceLayout.staticSamplers[i].shaderStages);
	}
	psoDesc.ResourceLayout.StaticSamplers = descs;

	Diligent::ShaderResourceVariableDesc* vars = new Diligent::ShaderResourceVariableDesc[aCreateInfo.resourceLayout.numVariables];
	for(uint32_t i = 0; i < aCreateInfo.resourceLayout.numVariables; i++)
	{
		vars[0].Name = aCreateInfo.resourceLayout.variables[0].name;
		vars[0].ShaderStages = static_cast<Diligent::SHADER_TYPE>(aCreateInfo.resourceLayout.variables[0].shaderStages);
		vars[0].Type = static_cast<Diligent::SHADER_RESOURCE_VARIABLE_TYPE>(aCreateInfo.resourceLayout.variables[0].type);
	}

	psoDesc.ResourceLayout.Variables = vars;

	Diligent::IRenderDevice* device = static_cast<Diligent::IRenderDevice*>(GraphicsContext::instance().getGraphicsDevice());
	device->CreatePipelineState(psoDesc, &mImpl->pipeline);
}

GraphicsPipeline::~GraphicsPipeline()
{
	delete mImpl;
}

void* GraphicsPipeline::getState() const
{
	return &*mImpl->pipeline;
}

void GraphicsPipeline::_createBuffer(const EShaderType aShader, const std::string& aName, void* aBuffer) const
{
	mImpl->pipeline->GetStaticVariableByName(static_cast<Diligent::SHADER_TYPE>(aShader), aName.c_str())->Set(static_cast<Diligent::IDeviceObject*>(aBuffer));
	mImpl->pipeline->CreateShaderResourceBinding(&mImpl->shaderBinding, true);

	mImpl->hasMaterial = true;
}

void* GraphicsPipeline::_getBinding() const
{
	return mImpl->shaderBinding;
}

void GraphicsPipeline::bind()
{
	Diligent::IDeviceContext* context = static_cast<Diligent::IDeviceContext*>(GraphicsContext::instance().getContext());
	context->SetPipelineState(mImpl->pipeline);

	if(mImpl->hasMaterial)
	{
		GraphicsContext::instance().commitShaderResources(mImpl->shaderBinding, EResourceStateTransitionMode::TRANSITION);
	}
	else
	{
		GraphicsContext::instance().commitShaderResources(nullptr, EResourceStateTransitionMode::TRANSITION);
	}
}