#include "Graphics/IndexBuffer.h"
#include <Graphics/GraphicsEngine/interface/Buffer.h>
#include <Common/interface/RefCntAutoPtr.h>
#include "Graphics/GraphicsContext.h"
#include <Graphics/GraphicsEngine/interface/RenderDevice.h>
#include <Graphics/GraphicsEngine/interface/DeviceContext.h>

class IndexBufferImpl
{
	public:
		Diligent::RefCntAutoPtr<Diligent::IBuffer> buffer;
};

IndexBuffer::IndexBuffer(const void* aData, const size_t aSize)
{
	mImpl = new IndexBufferImpl();
	mSize = aSize;

	Diligent::BufferDesc indBufferDesc;
	indBufferDesc.Name = "Index Buffer";
	indBufferDesc.Usage = Diligent::USAGE_STATIC;
	indBufferDesc.BindFlags = Diligent::BIND_INDEX_BUFFER;
	indBufferDesc.uiSizeInBytes = static_cast<uint32_t>(aSize);

	Diligent::BufferData iData;
	iData.pData = aData;
	iData.DataSize = static_cast<uint32_t>(aSize);

	Diligent::IRenderDevice* device = static_cast<Diligent::IRenderDevice*>(GraphicsContext::instance().getGraphicsDevice());
	device->CreateBuffer(indBufferDesc, &iData, &mImpl->buffer);
}

IndexBuffer::~IndexBuffer()
{
	delete mImpl;
}

void IndexBuffer::bind() const
{
	Diligent::IDeviceContext* context = static_cast<Diligent::IDeviceContext*>(GraphicsContext::instance().getContext());
	context->SetIndexBuffer(mImpl->buffer, 0, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION);
}


