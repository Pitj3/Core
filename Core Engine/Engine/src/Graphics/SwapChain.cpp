#include "Graphics/SwapChain.h"

#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include "Graphics/GraphicsEngineD3D11/interface/EngineFactoryD3D11.h"
#include "Graphics/GraphicsEngineD3D12/interface/EngineFactoryD3D12.h"
#include "Graphics/GraphicsEngineOpenGL/interface/EngineFactoryOpenGL.h"
#include "Graphics/GraphicsEngineVulkan/interface/EngineFactoryVk.h"

#include "Common/interface/RefCntAutoPtr.h"
#include "Graphics/GraphicsContext.h"
#include "Core/Application.h"

class SwapChainImpl
{
	public:
		Diligent::RefCntAutoPtr<Diligent::ISwapChain> swapchain;

		SwapChainInfo info;

		~SwapChainImpl()
		{

		}
};

SwapChain::SwapChain(const SwapChainInfo& aInfo)
{
	mImpl = new SwapChainImpl();

	mImpl->info = aInfo;

	Diligent::SwapChainDesc scoDesc;
	scoDesc.Width = aInfo.width;
	scoDesc.Height = aInfo.height;
	scoDesc.BufferCount = aInfo.bufferCount;
	scoDesc.DepthBufferFormat = static_cast<Diligent::TEXTURE_FORMAT>(aInfo.depthBufferFormat);
	scoDesc.ColorBufferFormat = static_cast<Diligent::TEXTURE_FORMAT>(aInfo.colorBufferFormat);
	scoDesc.DefaultDepthValue = aInfo.defaultDepthValue;
	scoDesc.DefaultStencilValue = aInfo.defaultStencilValue;
	scoDesc.IsPrimary = aInfo.isPrimary;
	scoDesc.Usage = static_cast<Diligent::SWAP_CHAIN_USAGE_FLAGS>(aInfo.usage);
	scoDesc.SamplesCount = aInfo.samplesCount;

	Diligent::IRenderDevice* device = static_cast<Diligent::IRenderDevice*>(GraphicsContext::instance().getGraphicsDevice());
	Diligent::IDeviceContext* context = static_cast<Diligent::IDeviceContext*>(GraphicsContext::instance().getContext());

	switch(GraphicsContext::instance().getType())
	{
		case EGraphicsContextType::Directx11: 
		{
			auto* apiFactory = Diligent::GetEngineFactoryD3D11();
			apiFactory->CreateSwapChainD3D11(device, context, scoDesc, Diligent::FullScreenModeDesc{}, glfwGetWin32Window(Application::instance().getWindow().getNativeWindow()), &mImpl->swapchain);
			
				break;
		}
		case EGraphicsContextType::Directx12: 
		{
			auto* apiFactory = Diligent::GetEngineFactoryD3D12();
			apiFactory->CreateSwapChainD3D12(device, context, scoDesc, Diligent::FullScreenModeDesc{}, glfwGetWin32Window(Application::instance().getWindow().getNativeWindow()), &mImpl->swapchain);

			break;
		}
		case EGraphicsContextType::OpenGL: break;
		case EGraphicsContextType::Vulkan: 
		{
			auto* apiFactory = Diligent::GetEngineFactoryVk();
			apiFactory->CreateSwapChainVk(device, context, scoDesc, glfwGetWin32Window(Application::instance().getWindow().getNativeWindow()), &mImpl->swapchain);
			
				break;
		}
		default: ;
	}

	
}

SwapChain::~SwapChain()
{
	delete mImpl;
}

void SwapChain::present(const uint32_t aSyncInterval) const
{
	mImpl->swapchain->Present(aSyncInterval);
}

void SwapChain::resize(const uint32_t aWidth, const uint32_t aHeight) const
{
	mImpl->swapchain->Resize(aWidth, aHeight);
}

SwapChainInfo SwapChain::getInfo() const
{
	return mImpl->info;
}
