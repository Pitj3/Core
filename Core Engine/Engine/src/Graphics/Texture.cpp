#include "Graphics/Texture.h"

#include <Common/interface/RefCntAutoPtr.h>
#include <Graphics/GraphicsEngine/interface/RenderDevice.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb/stb_image.h>

#include "Graphics/GraphicsEngine/interface/Texture.h"
#include "Graphics/GraphicsContext.h"


class TextureImpl
{
	public:
		Diligent::RefCntAutoPtr<Diligent::ITexture> texture;
};

Texture::Texture(const std::string& aPath)
{
	mImpl = new TextureImpl();

	int32_t width, height, channels;

	unsigned char* pixels = stbi_load(aPath.c_str(), &width, &height, &channels, STBI_rgb_alpha);

	Diligent::TextureDesc desc;
	desc.Width = width;
	desc.Height = height;
	desc.BindFlags = Diligent::BIND_SHADER_RESOURCE;
	desc.Format = Diligent::TEX_FORMAT_RGBA8_UNORM;
	desc.Type = Diligent::RESOURCE_DIM_TEX_2D;
	desc.Usage = Diligent::USAGE_DEFAULT;
	desc.MipLevels = 1;
	desc.CPUAccessFlags = Diligent::CPU_ACCESS_NONE;
	desc.MiscFlags = Diligent::MISC_TEXTURE_FLAG_NONE;

	Diligent::TextureData data;
	data.NumSubresources = 1;
	data.pSubResources = new Diligent::TextureSubResData[1];
	data.pSubResources[0].pData = pixels;
	data.pSubResources[0].Stride = width * sizeof(uint8_t) * 4;

	Diligent::IRenderDevice* device = static_cast<Diligent::IRenderDevice*>(GraphicsContext::instance().getGraphicsDevice());
	device->CreateTexture(desc, &data, &mImpl->texture);
	
	stbi_image_free(pixels);
}

Texture::~Texture()
{
	delete mImpl;
}

void* Texture::getTextureView(ETextureViewMode aMode) const
{
	return mImpl->texture->GetDefaultView(static_cast<Diligent::TEXTURE_VIEW_TYPE>(aMode));
}
