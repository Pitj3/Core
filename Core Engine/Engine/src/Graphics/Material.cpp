#include "Graphics/Material.h"
#include "Core/Log.h"
#include <glm/gtx/compatibility.hpp>

#include <Common/interface/RefCntAutoPtr.h>
#include <Graphics/GraphicsEngine/interface/Buffer.h>
#include "Graphics/GraphicsContext.h"
#include <Graphics/GraphicsEngine/interface/RenderDevice.h>
#include <Graphics/GraphicsEngine/interface/DeviceContext.h>
#include "Utils/StringUtils.h"

namespace detail
{
	uint32_t getParameterTypeSize(const EShaderParameterType aType)
	{
		switch(aType)
		{
			case EShaderParameterType::BOOL: return 1;
			case EShaderParameterType::UINT: return 4;
			case EShaderParameterType::INT: return 4;
			case EShaderParameterType::HALF: return 2;
			case EShaderParameterType::FLOAT: return 4;
			case EShaderParameterType::DOUBLE: return 8;
			case EShaderParameterType::BOOL2: return 2;
			case EShaderParameterType::BOOL3: return 3;
			case EShaderParameterType::BOOL4: return 4;
			case EShaderParameterType::UINT2: return 8;
			case EShaderParameterType::UINT3: return 12;
			case EShaderParameterType::UINT4: return 16;
			case EShaderParameterType::INT2: return 8;
			case EShaderParameterType::INT3: return 12;
			case EShaderParameterType::INT4: return 16;
			case EShaderParameterType::HALF2: return 4;
			case EShaderParameterType::HALF3: return 6;
			case EShaderParameterType::HALF4: return 8;
			case EShaderParameterType::FLOAT2: return 8;
			case EShaderParameterType::FLOAT3: return 12;
			case EShaderParameterType::FLOAT4: return 16;
			case EShaderParameterType::DOUBLE2: return 16;
			case EShaderParameterType::DOUBLE3: return 24;
			case EShaderParameterType::DOUBLE4: return 32;
			case EShaderParameterType::FLOAT2X2: return 16;
			case EShaderParameterType::FLOAT3X3: return 36;
			case EShaderParameterType::FLOAT4X4: return 64;
			default: return 0;
		}
	}
}

class MaterialImpl
{
	public:
		std::map<std::string, Diligent::RefCntAutoPtr<Diligent::IBuffer>> buffers;
		std::map<std::string, std::map<std::string, MaterialParameter*>> mParameters;
		std::map<std::string, uint32_t> mBufferSize;
		std::map<std::string, EShaderType> mBufferType;

		GraphicsPipeline* pipeline;
};

Material::Material(GraphicsPipeline* aPipeline)
{
	mImpl = new MaterialImpl();
	mImpl->pipeline = aPipeline;
}

Material::~Material()
{
	delete mImpl;
}

void Material::addBlock(const std::string& aName, const EShaderType aShaderType) const
{
	mImpl->mBufferType[aName] = aShaderType;
}

void Material::addParameter(const std::string& aName, const EShaderParameterType aType, void* aValue)
{
	const std::string blockName = StringUtils::split(aName, ':')[0];
	const std::string parameterName = StringUtils::split(aName, ':')[1];

	if (mImpl->mParameters[blockName].find(aName) == mImpl->mParameters[blockName].end())
	{
		MaterialParameter* parameter = new MaterialParameter();
		parameter->name = parameterName;
		parameter->type = aType;
		parameter->value = aValue;
		parameter->offset = mImpl->mBufferSize[blockName];

		mImpl->mParameters[blockName].insert({ parameterName, parameter });

		mImpl->mBufferSize[blockName] += detail::getParameterTypeSize(aType);
	}
	else
	{
		CORE_ERROR("Parameter with name ({0}) already exists!", parameterName);
	}
}

void Material::construct() const
{
	for(const auto& buffer : mImpl->mBufferSize)
	{
		Diligent::BufferDesc desc;
		desc.Name = "Material Buffer";
		desc.uiSizeInBytes = buffer.second;
		desc.Usage = Diligent::USAGE_DYNAMIC;
		desc.BindFlags = Diligent::BIND_UNIFORM_BUFFER;
		desc.CPUAccessFlags = Diligent::CPU_ACCESS_WRITE;

		Diligent::IRenderDevice* device = static_cast<Diligent::IRenderDevice*>(GraphicsContext::instance().getGraphicsDevice());
		device->CreateBuffer(desc, nullptr, &mImpl->buffers[buffer.first]);

		mImpl->pipeline->_createBuffer(mImpl->mBufferType[buffer.first], buffer.first, mImpl->buffers[buffer.first]);
	}
}

void Material::bind() const
{
	mImpl->pipeline->bind();
}

MaterialParameter* Material::getParameter(const std::string& aName) const
{
	const std::string blockName = StringUtils::split(aName, ':')[0];
	const std::string parameterName = StringUtils::split(aName, ':')[1];

	const auto iter = mImpl->mParameters[blockName].find(parameterName);
	if (iter != mImpl->mParameters[blockName].end())
	{
		return iter->second;
	}

	return nullptr;
}

void Material::setTexture(EShaderType aShaderType, const std::string& aLocation, Texture* aTexture) const
{
	static_cast<Diligent::IShaderResourceBinding*>(mImpl->pipeline->_getBinding())->GetVariableByName(static_cast<Diligent::SHADER_TYPE>(aShaderType), aLocation.c_str())->Set(static_cast<Diligent::IDeviceObject*>(aTexture->getTextureView(ETextureViewMode::VIEW_SHADER_RESOURCE)));
}

void Material::setParameter(const std::string& aName, void* aValue)
{
	const std::string blockName = StringUtils::split(aName, ':')[0];
	const std::string parameterName = StringUtils::split(aName, ':')[1];

	Diligent::IDeviceContext* device = static_cast<Diligent::IDeviceContext*>(GraphicsContext::instance().getContext());

	void* data = nullptr;
	device->MapBuffer(mImpl->buffers[blockName], Diligent::MAP_WRITE, Diligent::MAP_FLAG_DISCARD, data);

	char* ptr = static_cast<char*>(data);

	MaterialParameter* param = getParameter(aName);
	memcpy(ptr + param->offset, aValue, detail::getParameterTypeSize(param->type));

	device->UnmapBuffer(mImpl->buffers[blockName], Diligent::MAP_WRITE);
}

