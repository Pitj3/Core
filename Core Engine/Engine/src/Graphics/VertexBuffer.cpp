#include "Graphics/VertexBuffer.h"

#include "Graphics/GraphicsEngine/interface/InputLayout.h"
#include "Graphics/GraphicsEngine/interface/Buffer.h"
#include "Graphics/GraphicsContext.h"
#include <Graphics/GraphicsEngine/interface/RenderDevice.h>
#include <Graphics/GraphicsEngine/interface/DeviceContext.h>
#include "Common/interface/RefCntAutoPtr.h"

class VertexBufferImpl
{
	public:
		Diligent::RefCntAutoPtr<Diligent::IBuffer> buffer;
};

VertexBuffer::VertexBuffer(const void* aData, const size_t aSize)
{
	mImpl = new VertexBufferImpl();
	mSize = aSize;

	Diligent::BufferDesc vertBufferDesc;
	vertBufferDesc.Name = "Vertex Buffer";
	vertBufferDesc.Usage = Diligent::USAGE_STATIC;
	vertBufferDesc.BindFlags = Diligent::BIND_VERTEX_BUFFER;
	vertBufferDesc.uiSizeInBytes = static_cast<uint32_t>(aSize);

	Diligent::BufferData vData;
	vData.pData = aData;
	vData.DataSize = static_cast<uint32_t>(aSize);

	Diligent::IRenderDevice* device = static_cast<Diligent::IRenderDevice*>(GraphicsContext::instance().getGraphicsDevice());
	device->CreateBuffer(vertBufferDesc, &vData, &mImpl->buffer);
}

VertexBuffer::~VertexBuffer()
{
	delete mImpl;
}

void VertexBuffer::bind() const
{
	Diligent::IDeviceContext* context = static_cast<Diligent::IDeviceContext*>(GraphicsContext::instance().getContext());

	uint32_t offset = 0;
	Diligent::IBuffer* buffers[] = { mImpl->buffer };
	context->SetVertexBuffers(0, 1, buffers, &offset, Diligent::RESOURCE_STATE_TRANSITION_MODE_TRANSITION, Diligent::SET_VERTEX_BUFFERS_FLAG_RESET);
}
