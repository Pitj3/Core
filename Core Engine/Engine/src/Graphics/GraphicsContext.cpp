#include "Graphics/GraphicsContext.h"

#ifndef NOMINMAX
#   define NOMINMAX
#endif

#define GLFW_EXPOSE_NATIVE_WIN32
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include "Graphics/GraphicsEngineD3D11/interface/EngineFactoryD3D11.h"
#include "Graphics/GraphicsEngineD3D12/interface/EngineFactoryD3D12.h"
#include "Graphics/GraphicsEngineOpenGL/interface/EngineFactoryOpenGL.h"
#include "Graphics/GraphicsEngineVulkan/interface/EngineFactoryVk.h"

#include "Common/interface/RefCntAutoPtr.h"

#include "Core/Application.h"
#include "Graphics/Shader.h"
#include "Graphics/GraphicsPipeline.h"
#include "Graphics/SwapChain.h"

GraphicsContext* GraphicsContext::sInstance;

class GraphicsContextImpl
{
	public:
		Diligent::RefCntAutoPtr<Diligent::IRenderDevice> device;
		Diligent::RefCntAutoPtr<Diligent::IDeviceContext> immediateContext;

		SwapChain* swapchain;

		void initialize(const EGraphicsContextType& aContextType)
		{
			switch(aContextType)
			{
				case EGraphicsContextType::Directx11: 
				{
					const Diligent::EngineD3D11CreateInfo deviceAttribs;

					auto* f = Diligent::GetEngineFactoryD3D11();
					f->CreateDeviceAndContextsD3D11(deviceAttribs, &device, &immediateContext);

					break;
				}

				case EGraphicsContextType::Directx12: 
				{
					const Diligent::EngineD3D12CreateInfo deviceAttribs;

					auto* f = Diligent::GetEngineFactoryD3D12();
					f->CreateDeviceAndContextsD3D12(deviceAttribs, &device, &immediateContext);

					break;
				}
				case EGraphicsContextType::OpenGL: break;
				case EGraphicsContextType::Vulkan: 
				{
					const Diligent::EngineVkCreateInfo deviceAttribs;

					auto* f = Diligent::GetEngineFactoryVk();
					f->CreateDeviceAndContextsVk(deviceAttribs, &device, &immediateContext);

					break;
				}
				default: ;
			}

			
			SwapChainInfo info;
			info.width = Application::instance().getWindow().getWidth();
			info.height = Application::instance().getWindow().getHeight();
			info.samplesCount = 1;

			swapchain = new SwapChain(info);
		}

		~GraphicsContextImpl()
		{
			immediateContext->Flush();
			delete swapchain;
		}
};

GraphicsContext::GraphicsContext(const EGraphicsContextType& aContextType)
{
	sInstance = this;

	mImpl = new GraphicsContextImpl();
	mType = aContextType;

	mImpl->initialize(mType);
}

GraphicsContext::~GraphicsContext()
{
	delete mImpl;
}

void GraphicsContext::present() const
{
	mImpl->swapchain->present();
}

void GraphicsContext::clearRenderTarget(const float* aRGBA, const EResourceStateTransitionMode aTransitionMode) const
{
	mImpl->immediateContext->ClearRenderTarget(nullptr, aRGBA, 
		static_cast<Diligent::RESOURCE_STATE_TRANSITION_MODE>(aTransitionMode));
}

void GraphicsContext::clearDepthStencil(const uint32_t aClearFlags, const float aDepth, const uint8_t aStencil,
	const EResourceStateTransitionMode aTransitionMode) const
{
	mImpl->immediateContext->ClearDepthStencil(nullptr, 
		static_cast<Diligent::CLEAR_DEPTH_STENCIL_FLAGS>(aClearFlags), aDepth, aStencil, 
		static_cast<Diligent::RESOURCE_STATE_TRANSITION_MODE>(aTransitionMode));
}

void GraphicsContext::commitShaderResources(void* aBinding, const EResourceStateTransitionMode aTransitionMode) const
{
	mImpl->immediateContext->CommitShaderResources(static_cast<Diligent::IShaderResourceBinding*>(aBinding), static_cast<Diligent::RESOURCE_STATE_TRANSITION_MODE>(aTransitionMode));
}

void GraphicsContext::draw(DrawAttributes& aAttribs) const
{
	Diligent::DrawAttribs att;

	att.BaseVertex = aAttribs.baseVertex;
	att.FirstInstanceLocation = aAttribs.firstInstanceLocation;
	att.Flags = static_cast<Diligent::DRAW_FLAGS>(aAttribs.flags);
	att.IndexType = static_cast<Diligent::VALUE_TYPE>(aAttribs.indexType);
	att.IndirectAttribsBufferStateTransitionMode = static_cast<Diligent::RESOURCE_STATE_TRANSITION_MODE>(aAttribs.indirectAttribsBufferStateTransitionMode);
	att.IndirectDrawArgsOffset = aAttribs.indirectDrawArgsOffset;
	att.IsIndexed = aAttribs.isIndexed;
	att.NumInstances = aAttribs.numInstances;
	att.NumIndices = aAttribs.numIndices;
	att.NumVertices = aAttribs.numVertices;
	att.FirstIndexLocation = aAttribs.firstIndexLocation;
	att.FirstInstanceLocation = aAttribs.firstInstanceLocation;

	mImpl->immediateContext->Draw(att);
}

GraphicsContext& GraphicsContext::instance()
{
	return *sInstance;
}

void* GraphicsContext::getGraphicsDevice() const
{
	return mImpl->device;
}

void* GraphicsContext::getContext() const
{
	return &*mImpl->immediateContext;
}

SwapChain* GraphicsContext::getSwapChain() const
{
	return mImpl->swapchain;
}

bool GraphicsContext::isGl() const
{
	return mImpl->device->GetDeviceCaps().IsGLDevice();
}

EGraphicsContextType GraphicsContext::getType() const
{
	return mType;
}
