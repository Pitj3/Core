#include "Assets/MeshAsset.h"

#include <fx/gltf.h>
#include "Utils/StringUtils.h"

#include "Core/Log.h"

#include "Math/Vector2.h"
#include "Math/Vector3.h"
#include "Graphics/StaticMesh.h"
#include "Components/StaticMeshRenderer.h"

#include "ECS/EntityManager.h"

#include "Components/TransformComponent.h"

namespace detail
{
	struct BufferInfo
	{
		fx::gltf::Accessor const* accessor;

		uint8_t const* data;
		uint32_t dataStride;
		uint32_t totalSize;

		bool hasData() const noexcept
		{
			return data != nullptr;
		}
	};

	static uint32_t sGetAccessorTypeSize(fx::gltf::Accessor const& aAccessor)
	{
		switch (aAccessor.componentType)
		{
			case fx::gltf::Accessor::ComponentType::Byte:
			case fx::gltf::Accessor::ComponentType::UnsignedByte:
				return 1;

			case fx::gltf::Accessor::ComponentType::Short:
			case fx::gltf::Accessor::ComponentType::UnsignedShort:
				return 2;

			case fx::gltf::Accessor::ComponentType::Float:
			case fx::gltf::Accessor::ComponentType::UnsignedInt:
				return 4;

			default:
				return 0;
		}
	}

	static uint32_t sCalculateDataTypeSize(const fx::gltf::Accessor& aAccessor) noexcept
	{
		const uint32_t elementSize = sGetAccessorTypeSize(aAccessor);

		switch (aAccessor.type)
		{
			case fx::gltf::Accessor::Type::Mat2:
				return 4 * elementSize;
			case fx::gltf::Accessor::Type::Mat3:
				return 9 * elementSize;
			case fx::gltf::Accessor::Type::Mat4:
				return 16 * elementSize;
			case fx::gltf::Accessor::Type::Scalar:
				return elementSize;
			case fx::gltf::Accessor::Type::Vec2:
				return 2 * elementSize;
			case fx::gltf::Accessor::Type::Vec3:
				return 3 * elementSize;
			case fx::gltf::Accessor::Type::Vec4:
				return 4 * elementSize;
			default:
				return 0;
		}
	}

	static BufferInfo sGetData(fx::gltf::Document const& aDocument, fx::gltf::Accessor const& aAccessor)
	{
		fx::gltf::BufferView const& bufferView = aDocument.bufferViews[aAccessor.bufferView];
		fx::gltf::Buffer const& buffer = aDocument.buffers[bufferView.buffer];

		const uint32_t dataTypeSize = sCalculateDataTypeSize(aAccessor);
		return BufferInfo{ &aAccessor, &buffer.data[static_cast<uint64_t>(bufferView.byteOffset) + aAccessor.byteOffset], dataTypeSize, aAccessor.count * dataTypeSize };
	}
}

MeshAsset::MeshAsset(const std::string& aPath)
{
	mPath = aPath;
}

MeshAsset::~MeshAsset()
{
	
}

StaticMesh* MeshAsset::getMesh(const size_t aIndex) const
{
	return mMeshes[aIndex];
}

Entity* MeshAsset::constructEntityFromAsset()
{
	Entity* entity = EntityManager::instance().create("MeshEntity");

	for(auto node : mNodes)
	{
		Entity* child = EntityManager::instance().create("ChildMesh");

		child->transform->position = node->position;
		child->transform->rotation = node->rotation;
		child->transform->scale = node->scale;

		if(node->mesh != nullptr)
			child->addComponent<StaticMeshRenderer>(node->mesh);

		child->setParent(entity);
	}

	return entity;
}

void MeshAsset::_load()
{
	const bool isText = StringUtils::endsWith(mPath, "gltf");
	const bool isBinary = StringUtils::endsWith(mPath, "glb");

	const bool isGLTF = isText || isBinary;

	if(isGLTF)
	{
		fx::gltf::Document document;

		if (isText)
		{
			document = fx::gltf::LoadFromText(mPath);
		}

		if (isBinary)
		{
			document = fx::gltf::LoadFromBinary(mPath);
		}

		// TODO (Roderick): Make recursive
		for (const auto& node : document.nodes)
		{
			MeshNode* n = new MeshNode();
			n->mesh = nullptr;

			n->position = { node.translation[0], node.translation[1], node.translation[2] };
			n->rotation = { node.rotation[0], node.rotation[1], node.rotation[2], node.rotation[3] };
			n->scale = { node.scale[0], node.scale[1], node.scale[2] };

			if(node.mesh >= 0)
			{
				const auto& mesh = document.meshes[node.mesh];

				std::vector<Vector3f> positions;
				std::vector<Vector2f> uvs;
				std::vector<Vector3f> normals;
				std::vector<Vector3f> tangents;
				std::vector<Vector3f> binormals;

				std::vector<uint32_t> indices;

				for (const auto& primitive : mesh.primitives)
				{
					for (const auto& attribute : primitive.attributes)
					{
						if (attribute.first == "POSITION")
						{
							auto accessor = document.accessors[attribute.second];
							auto positionBuffer = detail::sGetData(document, accessor);

							if (positionBuffer.hasData())
							{
								for (uint32_t i = 0; i < positionBuffer.totalSize / sizeof(float); i += 3)
								{
									float x = ((float*)positionBuffer.data)[i];
									float y = ((float*)positionBuffer.data)[i + 1];
									float z = ((float*)positionBuffer.data)[i + 2];

									positions.push_back({ x, y, z });
								}
							}
						}
						else if (attribute.first == "TEXCOORD_0")
						{
							auto accessor = document.accessors[attribute.second];
							auto uvBuffer = detail::sGetData(document, accessor);

							if (uvBuffer.hasData())
							{
								for (uint32_t i = 0; i < uvBuffer.totalSize / sizeof(float); i += 2)
								{
									float x = ((float*)uvBuffer.data)[i];
									float y = ((float*)uvBuffer.data)[i + 1];

									uvs.push_back({ x, y });
								}
							}
						}
						else if (attribute.first == "NORMAL")
						{
							auto accessor = document.accessors[attribute.second];
							auto normalBuffer = detail::sGetData(document, accessor);

							if (normalBuffer.hasData())
							{
								for (uint32_t i = 0; i < normalBuffer.totalSize / sizeof(float); i += 3)
								{
									float x = ((float*)normalBuffer.data)[i];
									float y = ((float*)normalBuffer.data)[i + 1];
									float z = ((float*)normalBuffer.data)[i + 2];

									normals.push_back({ x, y, z });
								}
							}
						}
						else if (attribute.first == "TANGENT")
						{
							auto accessor = document.accessors[attribute.second];
							auto tangentBuffer = detail::sGetData(document, accessor);

							if (tangentBuffer.hasData())
							{
								for (uint32_t i = 0; i < tangentBuffer.totalSize / sizeof(float); i += 4)
								{
									float x = ((float*)tangentBuffer.data)[i];
									float y = ((float*)tangentBuffer.data)[i + 1];
									float z = ((float*)tangentBuffer.data)[i + 2];
									float w = ((float*)tangentBuffer.data)[i + 3];

									tangents.push_back({ x, y, z });
									binormals.push_back({ w, 0, 0 });
								}
							}
						}
					}

					auto indexAccessor = document.accessors[primitive.indices];
					auto indexBuffer = detail::sGetData(document, indexAccessor);

					if (indexBuffer.hasData())
					{
						for (uint32_t i = 0; i < indexBuffer.totalSize / sizeof(uint16_t); i++)
						{
							uint16_t idx = ((uint16_t*)indexBuffer.data)[i];

							indices.push_back({ static_cast<uint32_t>(idx) });
						}
					}

					if (primitive.material >= 0)
					{
						auto material = document.materials[primitive.material];

						// TODO (Roderick): Load textures from material
					}

					StaticMesh* staticMesh = new StaticMesh();
					staticMesh->positions = positions;
					staticMesh->uvs = uvs;
					staticMesh->normals = normals;
					staticMesh->tangents = tangents;
					staticMesh->binormals = binormals;

					staticMesh->indices = indices;

					staticMesh->recalculateBinormals();
					staticMesh->build();

					mMeshes.push_back(staticMesh);
					n->mesh = staticMesh;

					mNodes.push_back(n);
				}
			}
		}
	}
	else
	{
		
	}
}

