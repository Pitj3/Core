#include "Core/Application.h"
#include "Core/Log.h"
#include "Input/Input.h"

#include "Graphics/GraphicsPipeline.h"
#include "Core/FileSystem.h"

#include "Math/Matrix4.h"

#include "Graphics/StaticMesh.h"
#include "Components/StaticMeshRenderer.h"

#include "ECS/EntityManager.h"
#include "ECS/System.h"
#include "ECS/SystemManager.h"
#include "Assets/AssetManager.h"
#include "Assets/MeshAsset.h"
#include "Graphics/Material.h"

class RenderSystem final : public System
{
	public:
		RenderSystem() = default;

		void render() override
		{
			auto entities = EntityManager::instance().getComponentsByType<StaticMeshRenderer>();

			for(auto renderer : entities)
			{
				renderer->onRender();
			}
		}
};

Application* Application::sInstance;

class ApplicationImpl
{
	public:
		~ApplicationImpl()
		{
			delete pipeline;
			delete material;
		}

		GraphicsPipeline* pipeline;

		Material* material;

		StaticMesh* mesh;
		StaticMeshRenderer* renderer;
};

struct Vertex
{
	Vector3f pos;
	Vector2f uv;
};

Application::Application(const EGraphicsContextType aType)
{
	Log::construct();

	mImpl = new ApplicationImpl();

	sInstance = this;

	const WindowProps props("Core Engine", 1280, 720);
	mWindow = new Window(props);
	mWindow->setEventCallback(BIND_EVENT_FUNCTION(Application::onEvent));

	mContext = new GraphicsContext(aType);

	GraphicsPipelineStateInfo info;
	info.numRenderTargets = 1;
	info.RTVFormats[0] = mContext->getSwapChain()->getInfo().colorBufferFormat;
	info.DSVFormat = mContext->getSwapChain()->getInfo().depthBufferFormat;
	info.depthStencilState.depthEnable = true;
	info.depthStencilState.depthWriteEnable = true;
	info.depthStencilState.depthFunc = EComparisonFunction::FUNC_LESS_EQUAL;
	info.primitiveTopology = EPrimitiveTopology::TOPOLOGY_TRIANGLE_LIST;
	info.rasterizerState.cullMode = ECUllMode::MODE_BACK;
	info.rasterizerState.frontCounterClockwise = true;

	const std::string vsSource = FileSystem::instance().loadToString("data/effects/DefaultVS.hlsl");
	const std::string psSource = FileSystem::instance().loadToString("data/effects/DefaultPS.hlsl");

	Shader* vs = new Shader("TestVS", vsSource, EShaderType::TYPE_VERTEX, EShaderLanguage::LANGUAGE_HLSL);
	Shader* ps = new Shader("TestPS", psSource, EShaderType::TYPE_PIXEL, EShaderLanguage::LANGUAGE_HLSL);

	info.vs = vs;
	info.ps = ps;

	info.inputLayout.layoutElements = new LayoutElementInfo[5];
	info.inputLayout.layoutElements[0] = { 0, 0, 3, EValueType::VT_FLOAT32, false };
	info.inputLayout.layoutElements[1] = { 1, 0, 2, EValueType::VT_FLOAT32, false };
	info.inputLayout.layoutElements[2] = { 2, 0, 3, EValueType::VT_FLOAT32, false };
	info.inputLayout.layoutElements[3] = { 3, 0, 3, EValueType::VT_FLOAT32, false };
	info.inputLayout.layoutElements[4] = { 4, 0, 3, EValueType::VT_FLOAT32, false };
	info.inputLayout.numElements = 5;

	info.resourceLayout.defaultVariableType = EShaderResourceVariableType::RESOURCE_VARIABLE_TYPE_STATIC;

	ShaderResourceVariableInfo vars[] =
	{
		{EShaderType::TYPE_PIXEL, "g_Texture", EShaderResourceVariableType::RESOURCE_VARIABLE_TYPE_MUTABLE}
	};
	info.resourceLayout.variables = vars;
	info.resourceLayout.numVariables = 1;

	SamplerInfo linearClampInfo
	{
		EFilterType::TYPE_LINEAR, EFilterType::TYPE_LINEAR, EFilterType::TYPE_LINEAR,
		ETextureAddressMode::ADDRESS_CLAMP, ETextureAddressMode::ADDRESS_CLAMP, ETextureAddressMode::ADDRESS_CLAMP
	};

	StaticSamplerInfo samplers[] =
	{
		{EShaderType::TYPE_PIXEL, "g_Texture", linearClampInfo}
	};

	info.resourceLayout.staticSamplers = samplers;
	info.resourceLayout.numStaticSamplers = 1;

	Texture* texture = new Texture("data/textures/test.png");

	mImpl->pipeline = new GraphicsPipeline("Triangle", info);

	mImpl->material = new Material(mImpl->pipeline);
	mImpl->material->addBlock("Constants", EShaderType::TYPE_VERTEX);
	mImpl->material->addParameter("Constants:g_WorldViewProj", EShaderParameterType::FLOAT4X4, nullptr);
	mImpl->material->construct();

	mImpl->material->setTexture(EShaderType::TYPE_PIXEL, "g_Texture", texture);

	auto m = AssetManager::instance().load<MeshAsset>("Box", "data/models/Box.glb");
	auto e = m->constructEntityFromAsset();

	SystemManager::instance().addSystem<RenderSystem>();

	mRunning = true;
}

Application::~Application()
{
	delete mImpl;
	delete mContext;
	delete mWindow;
	
	sInstance = nullptr;
}

void Application::onEvent(Event& aEvent)
{
	EventDispatcher dispatcher(aEvent);
	dispatcher.dispatch<WindowCloseEvent>(BIND_EVENT_FUNCTION(Application::_onWindowClose));
	dispatcher.dispatch<KeyPressedEvent>(BIND_EVENT_FUNCTION(Application::_onKeyPressed));
	dispatcher.dispatch<KeyReleasedEvent>(BIND_EVENT_FUNCTION(Application::_onKeyReleased));
	dispatcher.dispatch<MouseMovedEvent>(BIND_EVENT_FUNCTION(Application::_onMouseMoved));
	dispatcher.dispatch<MouseButtonPressedEvent>(BIND_EVENT_FUNCTION(Application::_onMousePressed));
	dispatcher.dispatch<MouseButtonReleasedEvent>(BIND_EVENT_FUNCTION(Application::_onMouseReleased));
	dispatcher.dispatch<MouseScrolledEvent>(BIND_EVENT_FUNCTION(Application::_onMouseScrolled));
}

float timeelapsed = 0.0f;

void Application::run() const
{
	while(mRunning)
	{
		Matrix4f model = Matrix4f::identity();
		model = Matrix4f::translate(model, { 0, 0, 0 });
		Matrix4f projection = Matrix4f::perspective(3.14f / 4.f, 16.0f / 9.0f, 0.01f, 1000.0f);
		Matrix4f view = Matrix4f::lookAt({ 10, 10, 10 }, { 0, 0, 0 }, { 0, 1, 0 });

		Matrix4f mvp = projection * view * model;

		const float clearColor[] = { 0.350f,  0.350f,  0.350f, 1.0f };

		GraphicsContext::instance().clearRenderTarget(clearColor);
		GraphicsContext::instance().clearDepthStencil(1, 1.0f, 0);

		mImpl->material->setParameter("Constants:g_WorldViewProj", &mvp);
		mImpl->material->bind();

		SystemManager::instance().update();
		SystemManager::instance().render();

		mWindow->onUpdate();
		GraphicsContext::instance().present();
	}
}

bool Application::_onWindowClose(WindowCloseEvent& aEvent)
{
	mRunning = false;
	return true;
}

bool Application::_onKeyPressed(KeyPressedEvent& aEvent) const
{
	Input::_setKeyPressed(aEvent.getKeyCode());
	return false;
}

bool Application::_onKeyReleased(KeyReleasedEvent& aEvent) const
{
	Input::_setKeyReleased(aEvent.getKeyCode());
	return false;
}

bool Application::_onMouseMoved(MouseMovedEvent& aEvent) const
{
	Input::_setMousePosition(aEvent.getX(), aEvent.getY());
	return false;
}

bool Application::_onMousePressed(MouseButtonPressedEvent& aEvent) const
{
	Input::_setMousePressed(aEvent.getMouseButton());
	return false;
}

bool Application::_onMouseReleased(MouseButtonReleasedEvent& aEvent) const
{
	Input::_setMouseReleased(aEvent.getMouseButton());
	return false;
}

bool Application::_onMouseScrolled(MouseScrolledEvent& aEvent) const
{
	Input::_setMouseScroll(aEvent.getXOffset(), aEvent.getYOffset());
	return false;
}