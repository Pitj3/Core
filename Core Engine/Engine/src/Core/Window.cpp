#include "Core/Window.h"
#include "Core/Log.h"
#include "Core/Assert.h"
#include <Event\ApplicationEvent.h>

static bool sGlfwInitialized = false;

namespace detail
{
	static void sGlfwErrorCallback(const int32_t aError, const char* aDescription)
	{
		CORE_INTERNAL_ERROR("GLFW Error ({0}): {1}", aError, aDescription);
	}
}

Window::Window(const WindowProps& aProps)
{
	mData.title = aProps.title;
	mData.width = aProps.width;
	mData.height = aProps.height;

	CORE_INTERNAL_INFO("Creating window {0}, ({1}, {2})", mData.title, mData.width, mData.height);

	if(!sGlfwInitialized)
	{
		const int32_t success = glfwInit();
		CORE_INTERNAL_ASSERT(success, "Could not initialize GLFW!");
		glfwSetErrorCallback(detail::sGlfwErrorCallback);
		sGlfwInitialized = true;
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	mWindow = glfwCreateWindow(static_cast<int32_t>(mData.width), static_cast<int32_t>(mData.height), 
		mData.title.c_str(), nullptr, nullptr);

	glfwSetWindowUserPointer(mWindow, &mData);

	glfwSetWindowSizeCallback(mWindow, [](GLFWwindow* aWindow, const int32_t aWidth, const int32_t aHeight)
	{
		WindowData& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(aWindow));
		data.width = aWidth;
		data.height = aHeight;

		WindowResizeEvent e(aWidth, aHeight);
		data.eventCallback(e);
	});

	glfwSetWindowCloseCallback(mWindow, [](GLFWwindow* aWindow)
	{
		WindowData& data = *static_cast<WindowData*>(glfwGetWindowUserPointer(aWindow));
		WindowCloseEvent e;
		data.eventCallback(e);
	});
}

Window::~Window()
{
	glfwDestroyWindow(mWindow);
	glfwTerminate();
}

void Window::onUpdate() const
{
	glfwPollEvents();
}

uint32_t Window::getWidth() const
{
	return mData.width;
}

uint32_t Window::getHeight() const
{
	return mData.height;
}

void Window::setEventCallback(const EventCallbackFunction& aCallback)
{
	mData.eventCallback = aCallback;
}

void Window::setVSync(const bool aEnabled)
{
	glfwSwapInterval(aEnabled ? 1 : 0);

	mData.vSync = aEnabled;
}

bool Window::isVSync() const
{
	return mData.vSync;
}
