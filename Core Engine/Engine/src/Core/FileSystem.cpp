#include "Core/FileSystem.h"

#include <fstream>

#include "Core/Log.h"
#include "Core/Assert.h"

FileSystem::FileSystem()
{
	mMountedPath = "";
}

FileSystem& FileSystem::instance()
{
	static FileSystem* instance = new FileSystem();
	return *instance;
}

void FileSystem::mount(const Path& aPath)
{
	if (exists(aPath))
	{
		mMountedPath = aPath;
		mMountedPath += "/";

		CORE_INTERNAL_INFO("Path mounted: {0}", aPath);
		return;
	}

	CORE_INTERNAL_ERROR("Cannot mount path, directory not found: {0}", aPath);
}

void FileSystem::unmount()
{
	CORE_INTERNAL_INFO("Path unmounted");
	mMountedPath = "";
}

Path FileSystem::getMountedDirectory() const
{
	return mMountedPath;
}

File* FileSystem::load(const Path& aPath) const
{
	if (exists(aPath))
	{
		File* file = new File();
		const std::string data = loadToString(aPath);

		file->mData = data;

		Path fullPath = mMountedPath;
		fullPath += aPath;

		file->mPath = fullPath;

		return file;
	}

	CORE_INTERNAL_ERROR("File does not exist at path: {0}", aPath.string());

	return nullptr;
}

std::string FileSystem::loadToString(const Path& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	if (exists(loadPath))
	{
		std::ifstream stream(loadPath.string());
		std::string str((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
		stream.close();

		return str;
	}

	CORE_INTERNAL_ERROR("File does not exist at path: {0}", loadPath.string());

	return "";
}

std::vector<char> FileSystem::getBytes(const Path& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	if (exists(loadPath))
	{
		std::ifstream file(loadPath.c_str(), std::ios::ate | std::ios::binary);

		CORE_ASSERT(file.is_open(), "Failed to open file");

		const size_t fileSize = static_cast<size_t>(file.tellg());
		std::vector<char> buffer(fileSize);

		file.seekg(0);
		file.read(buffer.data(), fileSize);

		file.close();

		return buffer;
	}

	return std::vector<char>();
}

uintmax_t FileSystem::getSize(const Path& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	if (exists(loadPath))
	{
		if(isDirectory(loadPath))
		{
			CORE_WARN("Trying to get size on a directory, this is not supported in Core Engine!");
			return 0;
		}

		if(isFile(loadPath))
		{
			auto size = fs::file_size(loadPath);
		}
	}

	return 0;
}

bool FileSystem::exists(const Path& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	return fs::exists(loadPath);
}

bool FileSystem::isFile(const Path& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	return fs::is_regular_file(loadPath);
}

bool FileSystem::isDirectory(const Path& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	return fs::is_directory(loadPath);
}

File* FileSystem::create(const std::string& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	if (exists(loadPath))
	{
		return load(loadPath);
	}

	File* file = new File();
	file->mPath = loadPath;

	return file;
}

void FileSystem::createDirectory(const Path& aPath) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	if (!exists(loadPath))
	{
		fs::create_directory(loadPath);
	}
}

std::vector<FileInfo> FileSystem::getFilesInPath(const Path& aPath, const bool aRecursive) const
{
	Path loadPath = mMountedPath;
	loadPath += aPath;

	std::vector<FileInfo> files;

	if (aRecursive)
	{
		for (const auto& p : fs::recursive_directory_iterator(loadPath))
		{
			if (p.is_directory())
				continue;

			const auto& path = p.path();
			files.push_back({ path.stem().string(), path.extension().string(), path.filename().string(), path.parent_path().string(), path.root_path().string() });
		}
	}
	else
	{
		for (const auto& p : fs::directory_iterator(loadPath))
		{
			if (p.is_directory())
				continue;

			const auto& path = p.path();
			files.push_back({ path.stem().string(), path.extension().string(), path.filename().string(), path.parent_path().string(), path.root_path().string() });
		}
	}

	return files;
}
