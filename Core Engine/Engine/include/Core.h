#ifndef CORE_H__
#define CORE_H__

#ifndef PLATFORM_WIN32
#   define PLATFORM_WIN32 1
#endif

#ifndef D3D11_SUPPORTED
#   define D3D11_SUPPORTED 1
#endif

#ifndef D3D12_SUPPORTED
#   define D3D12_SUPPORTED 1
#endif

#ifndef GL_SUPPORTED
#   define GL_SUPPORTED 1
#endif

#include "Core/Assert.h"
#include "Core/Log.h"
#include "Core/File.h"
#include "Core/FileSystem.h"
#include "Core/Timer.h"
#include "Core/Window.h"
#include "Core/Application.h"
#include "Core/PoolAllocator.h"
#include "Core/Property.h"

#include "Input/Input.h"

#include "ECS/Component.h"
#include "ECS/ComponentView.h"
#include "ECS/Entity.h"
#include "ECS/EntityManager.h"
#include "ECS/System.h"
#include "ECS/SystemManager.h"

#include "Event/Event.h"

#include "Graphics/BufferLayout.h"
#include "Graphics/GraphicsContext.h"
#include "Graphics/GraphicsPipeline.h"
#include "Graphics/GraphicsTypes.h"
#include "Graphics/IndexBuffer.h"
#include "Graphics/Material.h"
#include "Graphics/Shader.h"
#include "Graphics/SwapChain.h"
#include "Graphics/VertexBuffer.h"

#include "Math/Matrix2.h"
#include "Math/Matrix3.h"
#include "Math/Matrix4.h"
#include "Math/Vector2.h"
#include "Math/Vector3.h"
#include "Math/Vector4.h"
#include "Math/Quaternion.h"

#include "Utils/StringUtils.h"

#endif // CORE_H__