#ifndef CORE_STATICMESHRENDERER_H__
#define CORE_STATICMESHRENDERER_H__

#include "ECS/Component.h"

#include "Graphics/StaticMesh.h"
#include "Graphics/GraphicsPipeline.h"

class StaticMeshRenderer final : public Component
{
	public:
		explicit StaticMeshRenderer(StaticMesh* aMesh);

		void onConstruct() override;
		void onRender() override;

	private:
		StaticMesh* mMesh;
		GraphicsPipeline* mPipeline;
};

#endif // CORE_STATICMESHRENDERER_H__