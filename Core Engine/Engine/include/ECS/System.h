#ifndef CORE_SYSTEM_H__
#define CORE_SYSTEM_H__

#include "Event/Event.h"

class System
{
	public:
		System() = default;
		virtual ~System() = default;

		virtual void initialize() {}

		virtual void update() {}
		virtual void lateUpdate() {}
		virtual void fixedUpdate() {}

		virtual void onEvent(Event& aEvent) {}

		virtual void preRender() {}
		virtual void render() {}
		virtual void postRender() {}

		virtual void dispose() {}

};

#endif // CORE_SYSTEM_H__