#ifndef CORE_TEXTURE_H__
#define CORE_TEXTURE_H__

#include <string>

enum class ETextureViewMode : uint8_t
{
	VIEW_UNDEFINED = 0,
	VIEW_SHADER_RESOURCE,
	VIEW_RENDER_TARGET,
	VIEW_DEPTH_STENCIL,
	VIEW_UNORDERED_ACCESS,
	VIEW_NUM_VIEWS
};

class TextureImpl;
class Texture
{
	public:
		Texture(const std::string& aPath);
		~Texture();

		void* getTextureView(ETextureViewMode aMode) const;

	private:
		TextureImpl* mImpl;
};

#endif // CORE_TEXTURE_H__