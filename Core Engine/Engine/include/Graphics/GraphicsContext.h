#ifndef CORE_GRAPHICSCONTEXT_H__
#define CORE_GRAPHICSCONTEXT_H__

#include <cstdint>

#include "Graphics/GraphicsTypes.h"
#include "Graphics/SwapChain.h"

class GraphicsContextImpl;
class GraphicsContext
{
	public:
		explicit GraphicsContext(const EGraphicsContextType& aContextType);
		~GraphicsContext();

		void present() const;

		// TODO (Roderick): Add textureview to both functions as parameter
		void clearRenderTarget(const float* aRGBA, const EResourceStateTransitionMode aTransitionMode = EResourceStateTransitionMode::TRANSITION) const;
		void clearDepthStencil(const uint32_t aClearFlags, const float aDepth, const uint8_t aStencil, const EResourceStateTransitionMode aTransitionMode = EResourceStateTransitionMode::TRANSITION) const;

		// TODO (Roderick): Add shaderresourcebinding as parameter
		void commitShaderResources(void* aBinding, const EResourceStateTransitionMode aTransitionMode = EResourceStateTransitionMode::TRANSITION) const;

		void draw(DrawAttributes& aAttribs) const;

		static GraphicsContext& instance();

		void* getGraphicsDevice() const;
		void* getContext() const;
		SwapChain* getSwapChain() const;

		bool isGl() const;

		EGraphicsContextType getType() const;

	private:
		static GraphicsContext* sInstance;

		EGraphicsContextType mType;
		GraphicsContextImpl* mImpl;
};

#endif // CORE_GRAPHICSCONTEXT_H__