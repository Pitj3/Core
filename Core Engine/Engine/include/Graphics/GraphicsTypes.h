#ifndef CORE_GRAPHICSTYPES_H__
#define CORE_GRAPHICSTYPES_H__

#include <cstdint>

#include "Utils/FlagEnum.h"

enum class EGraphicsContextType : uint32_t
{
	Directx11,
	Directx12,
	OpenGL,
	Vulkan
};

// TODO (Roderick): Finish this list
enum class EShaderParameterType : uint32_t
{
	BOOL,
	UINT,
	INT,
	HALF,
	FLOAT,
	DOUBLE,
	BOOL2,
	BOOL3,
	BOOL4,
	UINT2,
	UINT3,
	UINT4,
	INT2,
	INT3,
	INT4,
	HALF2,
	HALF3,
	HALF4,
	FLOAT2,
	FLOAT3,
	FLOAT4,
	DOUBLE2,
	DOUBLE3,
	DOUBLE4,
	FLOAT2X2,
	FLOAT3X3,
	FLOAT4X4
};

enum class EResourceStateTransitionMode : uint8_t
{
	NONE = 0,
	TRANSITION,
	VERIFY
};

DEFINE_FLAG_ENUM_OPERATORS(EResourceStateTransitionMode)

enum class EBlendFactor : int8_t
{
	FACTOR_UNDEFINED = 0,
	FACTOR_ZERO,
	FACTOR_ONE,
	FACTOR_SRC_COLOR,
	FACTOR_INV_SRC_COLOR,
	FACTOR_SRC_ALPHA,
	FACTOR_INV_SRC_ALPHA,
	FACTOR_DEST_ALPHA,
	FACTOR_INV_DEST_ALPHA,
	FACTOR_DEST_COLOR,
	FACTOR_INV_DEST_COLOR,
	FACTOR_SRC_ALPHA_SAT,
	FACTOR_BLEND_FACTOR,
	FACTOR_INV_BLEND_FACTOR,
	FACTOR_SRC1_COLOR,
	FACTOR_INV_SRC1_COLOR,
	FACTOR_SRC1_ALPHA,
	FACTOR_INV_SRC1_ALPHA,
	FACTOR_NUM_FACTORS
};

enum class EBlendOperation : int8_t
{
	OPERATION_UNDEFINED = 0,
	OPERATION_ADD,
	OPERATION_SUBTRACT,
	OPERATION_REV_SUBTRACT,
	OPERATION_MIN,
	OPERATION_MAX,
	OPERATION_NUM_OPERATIONS
};

enum class ELogicOp : int8_t
{
	OP_CLEAR = 0,
	OP_SET,
	OP_COPY,
	OP_COPY_INVERTED,
	OP_NOOP,
	OP_INVERT,
	OP_AND,
	OP_NAND,
	OP_OR,
	OP_NOR,
	OP_XOR,
	OP_EQUIV,
	OP_AND_REVERSE,
	OP_AND_INVERTED,
	OP_OR_REVERSE,
	OP_OR_INVERTED,
	OP_NUM_OPERATIONS
};

enum class EColorMask : int8_t
{
	MASK_RED = 1,
	MASK_GREEN = 2,
	MASK_BLUE = 4,
	MASK_ALPHA = 8,
	MASK_ALL = (((MASK_RED | MASK_GREEN) | MASK_BLUE) | MASK_ALPHA)
};

struct RenderTargetBlendInfo
{
	bool blendEnable = false;
	bool logicOperationEnable = false;

	EBlendFactor srcBlend = EBlendFactor::FACTOR_ONE;
	EBlendFactor dstBlend = EBlendFactor::FACTOR_ZERO;
	EBlendOperation blendOp = EBlendOperation::OPERATION_ADD;
	EBlendFactor srcBlendAlpha = EBlendFactor::FACTOR_ONE;
	EBlendFactor dstBlendAlpha = EBlendFactor::FACTOR_ZERO;
	EBlendOperation blendOpAlpha = EBlendOperation::OPERATION_ADD;
	ELogicOp logicOp = ELogicOp::OP_NOOP;
	EColorMask renderTargetWriteMask = EColorMask::MASK_ALL;
};

struct BlendStateInfo
{
	bool alphaToCoverageEnable = false;
	bool independentBlendEnable = false;
	static constexpr int maxRenderTargets = 8;

	RenderTargetBlendInfo renderTargets[maxRenderTargets];
};

enum class EFillMode : int8_t
{
	MODE_UNDEFINED = 0,
	MODE_WIREFRAME,
	MODE_SOLID,
	MODE_NUM_MODES
};

enum class ECUllMode : int8_t
{
	MODE_UNDEFINED = 0,
	MODE_NONE,
	MODE_FRONT,
	MODE_BACK,
	MODE_NUM_MODES
};

struct RasterizerStateInfo
{
	EFillMode fillMode = EFillMode::MODE_SOLID;
	ECUllMode cullMode = ECUllMode::MODE_BACK;

	bool frontCounterClockwise = false;
	bool depthClipEnable = true;
	bool scissorEnable = false;
	bool antialiasedEnable = false;
	int32_t depthBias = 0;
	float depthBiasClamp = 0.0f;
	float slopeScaledDepthBias = 0.0f;
};

enum class EComparisonFunction : uint8_t
{
	FUNC_UNKNOWN = 0,
	FUNC_NEVER,
	FUNC_LESS,
	FUNC_EQUAL,
	FUNC_LESS_EQUAL,
	FUNC_GREATER,
	FUNC_NOT_EQUAL,
	FUNC_GREATER_EQUAL,
	FUNC_ALWAYS,
	FUNC_NUM_FUNCTIONS
};

enum class EStencilOp : int8_t
{
	OP_UNDEFINED = 0,
	OP_KEEP = 1,
	OP_ZERO = 2,
	OP_REPLACE = 3,
	OP_INCR_SAT = 4,
	OP_DECR_SAT = 5,
	OP_INVERT = 6,
	OP_INCR_WRAP = 7,
	OP_DECR_WRAP = 8,
	OP_NUM_OPS
};

struct StencilOpInfo
{
	EStencilOp stencilFailOp = EStencilOp::OP_KEEP;
	EStencilOp stencilDepthFailOp = EStencilOp::OP_KEEP;
	EStencilOp stencilPassOp = EStencilOp::OP_KEEP;
	EComparisonFunction stencilFunc = EComparisonFunction::FUNC_ALWAYS;
};

struct DepthStencilStateInfo
{
	bool depthEnable = true;
	bool depthWriteEnable = true;

	EComparisonFunction depthFunc = EComparisonFunction::FUNC_LESS;

	bool stencilEnable = false;
	uint8_t stencilReadMask = 0xFF;
	uint8_t stencilWriteMask = 0xFF;

	StencilOpInfo frontFace;
	StencilOpInfo backFace;
};

enum class EValueType : uint8_t
{
	VT_UNDEFINED = 0,
	VT_INT8,         
	VT_INT16,        
	VT_INT32,        
	VT_UINT8,        
	VT_UINT16,       
	VT_UINT32,       
	VT_FLOAT16,      
	VT_FLOAT32,      
	VT_NUM_TYPES     
};

static constexpr uint32_t maxLayoutElements = 16;

struct LayoutElementInfo
{
	static constexpr uint32_t autoOffset = static_cast<uint32_t>(-1);
	static constexpr uint32_t autoStride = static_cast<uint32_t>(-1);

	uint32_t inputIndex = 0;
	uint32_t bufferSlot = 0;
	uint32_t numComponents = 0;
	
	EValueType valueType = EValueType::VT_FLOAT32;

	bool isNormalized = true;
	
	uint32_t relativeOffset = autoOffset;
	uint32_t stride = autoStride;

	enum class EFrequency : int32_t
	{
		FREQUENCY_UNDEFINED = 0,
		FREQUENCY_PER_VERTEX,
		FREQUENCY_PER_INSTANCE,
		FREQUENCY_NUM_FREQUENCIES
	};

	EFrequency frequency = EFrequency::FREQUENCY_PER_VERTEX;
	
	uint32_t instanceDataStepRate = 1;
};

struct InputLayoutInfo
{
	LayoutElementInfo* layoutElements = nullptr;

	uint32_t numElements = 0;
};

enum class EPrimitiveTopology : uint8_t
{
	TOPOLOGY_UNDEFINED = 0,
	TOPOLOGY_TRIANGLE_LIST,
	TOPOLOGY_TRIANGLE_STRIP,
	TOPOLOGY_POINT_LIST,
	TOPOLOGY_LINE_LIST,
	TOPOLOGY_1_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_2_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_3_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_4_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_5_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_6_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_7_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_8_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_9_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_10_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_11_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_12_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_13_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_14_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_15_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_16_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_17_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_18_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_19_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_20_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_21_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_22_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_23_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_24_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_25_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_26_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_27_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_28_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_29_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_30_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_31_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_32_CONTROL_POINT_PATCHLIST,
	TOPOLOGY_NUM_TOPOLOGIES
};

enum class ETextureFormat : uint16_t
{
	FORMAT_UNKNOWN = 0,
	FORMAT_RGBA32_TYPELESS,
	FORMAT_RGBA32_FLOAT,
	FORMAT_RGBA32_UINT,
	FORMAT_RGBA32_SINT,
	FORMAT_RGB32_TYPELESS,
	FORMAT_RGB32_FLOAT,
	FORMAT_RGB32_UINT,
	FORMAT_RGB32_SINT,
	FORMAT_RGBA16_TYPELESS,
	FORMAT_RGBA16_FLOAT,
	FORMAT_RGBA16_UNORM,
	FORMAT_RGBA16_UINT,
	FORMAT_RGBA16_SNORM,
	FORMAT_RGBA16_SINT,
	FORMAT_RG32_TYPELESS,
	FORMAT_RG32_FLOAT,
	FORMAT_RG32_UINT,
	FORMAT_RG32_SINT,
	FORMAT_R32G8X24_TYPELESS,
	FORMAT_D32_FLOAT_S8X24_UINT,
	FORMAT_R32_FLOAT_X8X24_TYPELESS,
	FORMAT_X32_TYPELESS_G8X24_UINT,
	FORMAT_RGB10A2_TYPELESS,
	FORMAT_RGB10A2_UNORM,
	FORMAT_RGB10A2_UINT,
	FORMAT_R11G11B10_FLOAT,
	FORMAT_RGBA8_TYPELESS,
	FORMAT_RGBA8_UNORM,
	FORMAT_RGBA8_UNORM_SRGB,
	FORMAT_RGBA8_UINT,
	FORMAT_RGBA8_SNORM,
	FORMAT_RGBA8_SINT,
	FORMAT_RG16_TYPELESS,
	FORMAT_RG16_FLOAT,
	FORMAT_RG16_UNORM,
	FORMAT_RG16_UINT,
	FORMAT_RG16_SNORM,
	FORMAT_RG16_SINT,
	FORMAT_R32_TYPELESS,
	FORMAT_D32_FLOAT,
	FORMAT_R32_FLOAT,
	FORMAT_R32_UINT,
	FORMAT_R32_SINT,
	FORMAT_R24G8_TYPELESS,
	FORMAT_D24_UNORM_S8_UINT,
	FORMAT_R24_UNORM_X8_TYPELESS,
	FORMAT_X24_TYPELESS_G8_UINT,
	FORMAT_RG8_TYPELESS,
	FORMAT_RG8_UNORM,
	FORMAT_RG8_UINT,
	FORMAT_RG8_SNORM,
	FORMAT_RG8_SINT,
	FORMAT_R16_TYPELESS,
	FORMAT_R16_FLOAT,
	FORMAT_D16_UNORM,
	FORMAT_R16_UNORM,
	FORMAT_R16_UINT,
	FORMAT_R16_SNORM,
	FORMAT_R16_SINT,
	FORMAT_R8_TYPELESS,
	FORMAT_R8_UNORM,
	FORMAT_R8_UINT,
	FORMAT_R8_SNORM,
	FORMAT_R8_SINT,
	FORMAT_A8_UNORM,
	FORMAT_R1_UNORM,
	FORMAT_RGB9E5_SHAREDEXP,
	FORMAT_RG8_B8G8_UNORM,
	FORMAT_G8R8_G8B8_UNORM,
	FORMAT_BC1_TYPELESS,
	FORMAT_BC1_UNORM,
	FORMAT_BC1_UNORM_SRGB,
	FORMAT_BC2_TYPELESS,
	FORMAT_BC2_UNORM,
	FORMAT_BC2_UNORM_SRGB,
	FORMAT_BC3_TYPELESS,
	FORMAT_BC3_UNORM,
	FORMAT_BC3_UNORM_SRGB,
	FORMAT_BC4_TYPELESS,
	FORMAT_BC4_UNORM,
	FORMAT_BC4_SNORM,
	FORMAT_BC5_TYPELESS,
	FORMAT_BC5_UNORM,
	FORMAT_BC5_SNORM,
	FORMAT_B5G6R5_UNORM,
	FORMAT_B5G5R5A1_UNORM,
	FORMAT_BGRA8_UNORM,
	FORMAT_BGRX8_UNORM,
	FORMAT_R10G10B10_XR_BIAS_A2_UNORM,
	FORMAT_BGRA8_TYPELESS,
	FORMAT_BGRA8_UNORM_SRGB,
	FORMAT_BGRX8_TYPELESS,
	FORMAT_BGRX8_UNORM_SRGB,
	FORMAT_BC6H_TYPELESS,
	FORMAT_BC6H_UF16,
	FORMAT_BC6H_SF16,
	FORMAT_BC7_TYPELESS,
	FORMAT_BC7_UNORM,
	FORMAT_BC7_UNORM_SRGB,
	FORMAT_NUM_FORMATS
};

struct SampleInfo
{
	uint8_t count = 1;
	uint8_t quality = 0;
};

enum class ESwapChainUsage : uint32_t
{
	USAGE_NONE = 0x00L,
	USAGE_RENDER_TARGET = 0x01L,
	USAGE_SHADER_INPUT = 0x02L,
	USAGE_COPY_SOURCE = 0x04L
};
DEFINE_FLAG_ENUM_OPERATORS(ESwapChainUsage)

enum class EDrawFlags : uint8_t
{
	FLAG_NONE = 0x00,
	FLAG_VERIFY_STATES = 0x01,
	FLAG_VERIFY_DRAW_ATTRIBS = 0x02,
	FLAG_VERIFY_RENDER_TARGETS = 0x04,
	FLAG_VERIFY_ALL = FLAG_VERIFY_STATES | FLAG_VERIFY_DRAW_ATTRIBS | FLAG_VERIFY_RENDER_TARGETS
};
DEFINE_FLAG_ENUM_OPERATORS(EDrawFlags)

struct DrawAttributes
{
	union
	{
		uint32_t numVertices = 0;
		uint32_t numIndices;
	};

	bool isIndexed = false;

	EValueType indexType = EValueType::VT_UNDEFINED;

	EDrawFlags flags = EDrawFlags::FLAG_NONE;
	EResourceStateTransitionMode indirectAttribsBufferStateTransitionMode = EResourceStateTransitionMode::NONE;

	uint32_t numInstances = 1;

	uint32_t baseVertex = 0;

	uint32_t indirectDrawArgsOffset = 0;

	union
	{
		uint32_t startVertexLocation = 0;
		uint32_t firstIndexLocation;
	};

	uint32_t firstInstanceLocation = 0;

	// TODO (Roderick): Implement buffer abstraction
	//IBuffer* pIndirectDrawAttribs = nullptr;
};

/// Describes the type of the shader resource variable
enum class EShaderResourceVariableType : uint8_t
{
	RESOURCE_VARIABLE_TYPE_STATIC = 0,
	RESOURCE_VARIABLE_TYPE_MUTABLE,
	RESOURCE_VARIABLE_TYPE_DYNAMIC,
	RESOURCE_VARIABLE_TYPE_NUM_TYPES
};

enum class EShaderLanguage : uint32_t
{
	LANGUAGE_DEFAULT = 0,
	LANGUAGE_HLSL,
	LANGUAGE_GLSL
};

enum class EShaderType : uint32_t
{
	TYPE_UNKNOWN = 0x000,
	TYPE_VERTEX = 0x001,
	TYPE_PIXEL = 0x002,
	TYPE_GEOMETRY = 0x004,
	TYPE_HULL = 0x008,
	TYPE_DOMAIN = 0x010,
	TYPE_COMPUTE = 0x020
};

DEFINE_FLAG_ENUM_OPERATORS(EShaderType)

struct ShaderResourceVariableInfo
{
	EShaderType shaderStages = EShaderType::TYPE_UNKNOWN;

	const char* name = nullptr;

	EShaderResourceVariableType type = EShaderResourceVariableType::RESOURCE_VARIABLE_TYPE_STATIC;
};

enum class EFilterType : uint8_t
{
	TYPE_UNKNOWN = 0,          
	TYPE_POINT,                
	TYPE_LINEAR,               
	TYPE_ANISOTROPIC,          
	TYPE_COMPARISON_POINT,     
	TYPE_COMPARISON_LINEAR,    
	TYPE_COMPARISON_ANISOTROPIC,
	TYPE_MINIMUM_POINT,        
	TYPE_MINIMUM_LINEAR,       
	TYPE_MINIMUM_ANISOTROPIC,  
	TYPE_MAXIMUM_POINT,        
	TYPE_MAXIMUM_LINEAR,       
	TYPE_MAXIMUM_ANISOTROPIC,  
	TYPE_NUM_FILTERS           
};

enum class ETextureAddressMode : uint8_t
{
	ADDRESS_UNKNOWN = 0,
	ADDRESS_WRAP = 1,
	ADDRESS_MIRROR = 2,
	ADDRESS_CLAMP = 3,
	ADDRESS_BORDER = 4,
	ADDRESS_MIRROR_ONCE = 5,
	ADDRESS_NUM_MODES
};

struct SamplerInfo
{
	EFilterType minFilter = EFilterType::TYPE_LINEAR;
	EFilterType magFilter = EFilterType::TYPE_LINEAR;
	EFilterType mipFilter = EFilterType::TYPE_LINEAR;
	ETextureAddressMode addressU = ETextureAddressMode::ADDRESS_CLAMP;
	ETextureAddressMode addressV = ETextureAddressMode::ADDRESS_CLAMP;
	ETextureAddressMode addressW = ETextureAddressMode::ADDRESS_CLAMP;
	float mipLODBias = 0;
	uint32_t maxAnisotropy = 0;
	EComparisonFunction comparisonFunc = EComparisonFunction::FUNC_NEVER;
	float borderColor[4] = { 0, 0, 0, 0 };
	float minLOD = 0;
	float maxLOD = +3.402823466e+38F;
};

struct StaticSamplerInfo
{
	EShaderType shaderStages = EShaderType::TYPE_UNKNOWN;
	const char* samplerOrTextureName = nullptr;
	SamplerInfo desc;
};

struct PipelineResourceLayoutInfo
{
	EShaderResourceVariableType defaultVariableType = EShaderResourceVariableType::RESOURCE_VARIABLE_TYPE_STATIC;
	
	uint32_t numVariables = 0;
	const ShaderResourceVariableInfo* variables = nullptr;

	uint32_t numStaticSamplers = 0;
	const StaticSamplerInfo* staticSamplers = nullptr;
};

#endif // CORE_GRAPHICSTYPES_H__