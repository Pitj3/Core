#ifndef CORE_SWAPCHAIN_H__
#define CORE_SWAPCHAIN_H__

#include <cstdint>
#include "Graphics/GraphicsTypes.h"

struct SwapChainInfo
{
	uint32_t width = 0;
	uint32_t height = 0;

	ETextureFormat colorBufferFormat = ETextureFormat::FORMAT_RGBA8_UNORM_SRGB;
	ETextureFormat depthBufferFormat = ETextureFormat::FORMAT_D32_FLOAT;

	ESwapChainUsage usage = ESwapChainUsage::USAGE_RENDER_TARGET;

	uint32_t samplesCount = 1;
	uint32_t bufferCount = 2;
	float defaultDepthValue = 1.0f;
	uint8_t defaultStencilValue = 0;

	bool isPrimary = true;
};

class SwapChainImpl;
class SwapChain
{
	public:
		explicit SwapChain(const SwapChainInfo& aInfo);
		~SwapChain();

		void present(const uint32_t aSyncInterval = 1) const;
		void resize(const uint32_t aWidth, const uint32_t aHeight) const;

		SwapChainInfo getInfo() const;

	private:
		SwapChainImpl* mImpl;
};

#endif // CORE_SWAPCHAIN_H__