#ifndef CORE_STATICMESH_H__
#define CORE_STATICMESH_H__

#include "Utils/DynamicArray.h"
#include "Math/Vector2.h"
#include "Math/Vector3.h"

#include "Graphics/VertexBuffer.h"
#include "Graphics/IndexBuffer.h"

class StaticMesh
{
	friend class StaticMeshRenderer;
	public:
		StaticMesh();
		~StaticMesh();

		void build();
		void recalculateBounds();
		void recalculateNormals();
		void recalculateBinormals();

		DynamicArray<Vector3f> positions;
		DynamicArray<Vector2f> uvs;
		DynamicArray<Vector3f> normals;
		DynamicArray<Vector3f> tangents;
		DynamicArray<Vector3f> binormals;

		DynamicArray<uint32_t> indices;

	private:
		VertexBuffer* mVertexBuffer;
		IndexBuffer* mIndexBuffer;
};

#endif // CORE_STATICMESH_H__