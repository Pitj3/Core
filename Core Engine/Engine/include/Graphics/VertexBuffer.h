#ifndef CORE_VERTEXBUFFER_H__
#define CORE_VERTEXBUFFER_H__

class VertexBufferImpl;
class VertexBuffer
{
	public:
		VertexBuffer(const void* aData, const size_t aSize);
		~VertexBuffer();

		void bind() const;

	private:
		VertexBufferImpl* mImpl;
		size_t mSize;
};

#endif // CORE_VERTEXBUFFER_H__