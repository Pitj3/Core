#ifndef CORE_SHADER_H__
#define CORE_SHADER_H__

#include <string>
#include "Graphics/GraphicsTypes.h"

class ShaderImpl;
class Shader
{
	public:
		explicit Shader(const std::string& aName, const std::string& aSource, const EShaderType aType, const EShaderLanguage aLanguage = EShaderLanguage::LANGUAGE_DEFAULT);
		~Shader();

		void* getProgram() const;

	private:
		ShaderImpl* mImpl;
};

#endif // CORE_SHADER_H__