#ifndef CORE_MATERIALCOMPONENT_H__
#define CORE_MATERIALCOMPONENT_H__

#include <string>
#include <map>

#include "ECS/Component.h"
#include "Graphics/GraphicsTypes.h"
#include "Graphics/GraphicsPipeline.h"
#include <vector>

struct MaterialParameter
{
	std::string name;
	EShaderParameterType type;
	uint32_t offset;
	void* value;
};

class MaterialImpl;
class Material
{
	public:
		explicit Material(GraphicsPipeline* aPipeline);
		~Material();

		void addBlock(const std::string& aName, const EShaderType aShaderType) const;
		void addParameter(const std::string& aName, const EShaderParameterType aType, void* aValue);
		void setParameter(const std::string& aName, void* aValue);
		MaterialParameter* getParameter(const std::string& aName) const;

		void setTexture(EShaderType aShaderType, const std::string& aLocation, Texture* aTexture) const;

		void construct() const;

		void bind() const;

	private:
		MaterialImpl* mImpl;
};

#endif // CORE_MATERIALCOMPONENT_H__