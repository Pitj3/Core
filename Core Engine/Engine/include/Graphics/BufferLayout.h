#ifndef CORE_BUFFERLAYOUT_H__
#define CORE_BUFFERLAYOUT_H__

#include <string>

#include "Graphics/GraphicsTypes.h"
#include "Core/Assert.h"
#include "Math/Vector2.h"
#include "Math/Vector4.h"
#include "Math/Matrix2.h"
#include "Math/Matrix3.h"
#include "Math/Matrix4.h"

struct BufferLayoutElement
{
	std::string name;
	EShaderParameterType type;
	size_t size;
	uint32_t count;
	size_t offset;
	bool normalized;
};

class BufferLayout
{
	public:
		BufferLayout() = default;
		~BufferLayout() = default;

		const std::vector<BufferLayoutElement>& getLayout() const { return mLayout; }
		size_t getStride() const { return mSize; }

		template<typename T>
		void push(const std::string& aName, const uint32_t aCount = 1, const bool aNormalize = false)
		{
			CORE_ASSERT(false);
		}
	private:
		size_t mSize = 0;
		std::vector<BufferLayoutElement> mLayout;

		void _push(const std::string& aName, const EShaderParameterType& aType, const size_t aSize, const uint32_t aCount, const bool aNormalized)
		{
			mLayout.push_back({ aName, aType, aSize, aCount, mSize, aNormalized });
			mSize += aSize * aCount;
		}
};

template<>
inline void BufferLayout::push<uint32_t>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::UINT, sizeof(uint32_t), aCount, aNormalized);
}

template<>
inline void BufferLayout::push<int32_t>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::INT, sizeof(int32_t), aCount, aNormalized);
}

template<>
inline void BufferLayout::push<float>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::FLOAT, sizeof(float), aCount, aNormalized);
}

template<>
inline void BufferLayout::push<double>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::DOUBLE, sizeof(double), aCount, aNormalized);
}

template<>
inline void BufferLayout::push<Vector2f>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::FLOAT2, sizeof(float) * 2, aCount, aNormalized);
}

template<>
inline void BufferLayout::push<Vector3f>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::FLOAT3, sizeof(float) * 3, aCount, aNormalized);
}

template<>
inline void BufferLayout::push<Vector4f>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::FLOAT4, sizeof(float) * 4, aCount, aNormalized);
}

template<>
inline void BufferLayout::push<Matrix2f>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::FLOAT2X2, sizeof(float) * 4, aCount, aNormalized);
}

template<>
inline void BufferLayout::push<Matrix3f>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::FLOAT3X3, sizeof(float) * 9, aCount, aNormalized);
}

template<>
inline void BufferLayout::push<Matrix4f>(const std::string& aName, const uint32_t aCount, const bool aNormalized)
{
	_push(aName, EShaderParameterType::FLOAT4X4, sizeof(float) * 16, aCount, aNormalized);
}

#endif // CORE_BUFFERLAYOUT_H__