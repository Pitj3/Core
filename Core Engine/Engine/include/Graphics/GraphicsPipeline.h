#ifndef CORE_GRAPHICSPIPELINE_H__
#define CORE_GRAPHICSPIPELINE_H__

#include "Graphics/Shader.h"
#include "Graphics/GraphicsTypes.h"

//#include "Graphics/Material.h"
#include "Graphics/Texture.h"

struct GraphicsPipelineStateInfo
{
	Shader* vs = nullptr;
	Shader* ps = nullptr;
	Shader* ds = nullptr;
	Shader* hs = nullptr;
	Shader* gs = nullptr;

	BlendStateInfo blendDesc;

	uint32_t sampleMask = 0xFFFFFFFF;

	RasterizerStateInfo rasterizerState;
	DepthStencilStateInfo depthStencilState;

	InputLayoutInfo inputLayout;

	PipelineResourceLayoutInfo resourceLayout;

	EPrimitiveTopology primitiveTopology = EPrimitiveTopology::TOPOLOGY_TRIANGLE_LIST;

	uint8_t numViewports = 1;
	uint8_t numRenderTargets = 0;

	ETextureFormat RTVFormats[8] = {};
	ETextureFormat DSVFormat = ETextureFormat::FORMAT_UNKNOWN;

	SampleInfo sampleInfo;

	uint32_t nodeMask = 0;
};

class GraphicsPipelineImpl;
class GraphicsPipeline
{
	friend class Material;
	public:
		GraphicsPipeline(const std::string& aName, const GraphicsPipelineStateInfo& aCreateInfo);
		~GraphicsPipeline();

		void* getState() const;

		void bind();

	private:
		GraphicsPipelineImpl* mImpl;

		void _createBuffer(const EShaderType aShader, const std::string& aName, void* aBuffer) const;
		void* _getBinding() const;
};


#endif // CORE_GRAPHICSPIPELINE_H__