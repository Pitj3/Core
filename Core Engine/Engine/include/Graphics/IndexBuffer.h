#ifndef CORE_INDEXBUFFER_H__
#define CORE_INDEXBUFFER_H__

class IndexBufferImpl;
class IndexBuffer
{
	public:
		IndexBuffer(const void* aData, const size_t aSize);
		~IndexBuffer();

		void bind() const;

	private:
		IndexBufferImpl* mImpl;
		size_t mSize;
};

#endif // CORE_INDEXBUFFER_H__