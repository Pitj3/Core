#ifndef CORE_APPLICATION_H__
#define CORE_APPLICATION_H__

#include "Core/Window.h"

#include "Event/Event.h"
#include "Event/KeyEvents.h"
#include "Event/ApplicationEvent.h"
#include "Event/MouseEvent.h"
#include "Graphics/GraphicsContext.h"

class ApplicationImpl;
class Application
{
	public:
		explicit Application(const EGraphicsContextType aType = EGraphicsContextType::Directx11);
		~Application();

		void onEvent(Event& aEvent);

		void run() const;

		Window& getWindow() const { return *mWindow; }
		static Application& instance() { return *sInstance; }

	private:
		ApplicationImpl* mImpl;

		static Application* sInstance;
		Window* mWindow;

		GraphicsContext* mContext;

		bool mRunning;

		// Events
		bool _onWindowClose(WindowCloseEvent& aEvent);
		bool _onKeyPressed(KeyPressedEvent& aEvent) const;
		bool _onKeyReleased(KeyReleasedEvent& aEvent) const;
		bool _onMouseMoved(MouseMovedEvent& aEvent) const;
		bool _onMousePressed(MouseButtonPressedEvent& aEvent) const;
		bool _onMouseReleased(MouseButtonReleasedEvent& aEvent) const;
		bool _onMouseScrolled(MouseScrolledEvent& aEvent) const;
};

#endif // CORE_APPLICATION_H__