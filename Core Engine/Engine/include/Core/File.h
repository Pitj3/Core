#ifndef CORE_FILE_H__
#define CORE_FILE_H__

#include <string>
#include <filesystem>

namespace fs = std::filesystem;
using Path = fs::path;

enum class EFileSaveMode : uint32_t
{
	Truncate,
	Append
};

class File
{
	friend class FileSystem;
	public:
		File();
		~File();

		void write(const char* aData);
		void write(const std::string& aData);

		void save(const Path& aPath, const EFileSaveMode& aMode = EFileSaveMode::Truncate);
		void save(const EFileSaveMode& aMode = EFileSaveMode::Truncate) const;

		size_t size() const;
		std::string data() const;

	private:
		Path mPath;
		std::string mData;
};

#endif // CORE_FILE_H__