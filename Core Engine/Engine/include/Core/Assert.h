#ifndef CORE_ASSERT_H__
#define CORE_ASSERT_H__

#include <memory>

#include "Core/Log.h"

#if defined(CORE_PLATFORM_WINDOWS)
	#define DEBUG_BREAK __debugbreak()
#elif defined(CORE_PLATFORM_LINUX)
	#include <signal.h>
	#define DEBUG_BREAK raise(SIGTRAP)
	#include <signal.h>
#elif defined(CORE_PLATFORM_MAC)
	#define DEBUG_BREAK raise(SIGTRAP)
#endif

#if defined(CORE_ENABLE_ASSERTS)
	#define CORE_ASSERT(x, ...) if(!(x)) { CORE_ERROR("Assertion Failed! {0}", __VA_ARGS__); DEBUG_BREAK; }
	#define CORE_INTERNAL_ASSERT(x, ...) if(!(x)) { CORE_INTERNAL_ERROR("Assertion Failed! {0}", __VA_ARGS__); DEBUG_BREAK; }
#else
	#define CORE_ASSERT(x, ...)
	#define CORE_INTERNAL_ASSERT(x, ...)
#endif // CORE_ENABLE_ASSERTS

#endif // CORE_ASSERT_H__