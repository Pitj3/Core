#ifndef CORE_LOG_H__
#define CORE_LOG_H__

#pragma warning(push)
#pragma warning(disable: 4251)

#include <memory>

#include <spdlog/spdlog.h>
#include <spdlog/fmt/ostr.h>

class Log
{
	public:
		static void construct();

		inline static std::shared_ptr<spdlog::logger>& getCoreLogger()
		{
			return sCoreLogger;
		}

		inline static std::shared_ptr<spdlog::logger>& getClientLogger()
		{
			return sClientLogger;
		}

	private:
		static std::shared_ptr<spdlog::logger> sCoreLogger;
		static std::shared_ptr<spdlog::logger> sClientLogger;
};

// Core log macros
#define CORE_INTERNAL_TRACE(...)	Log::getCoreLogger()->trace(__VA_ARGS__)
#define CORE_INTERNAL_INFO(...)		Log::getCoreLogger()->info(__VA_ARGS__)
#define CORE_INTERNAL_WARN(...)		Log::getCoreLogger()->warn(__VA_ARGS__)
#define CORE_INTERNAL_ERROR(...)	Log::getCoreLogger()->error(__VA_ARGS__)
#define CORE_INTERNAL_CRITICAL(...)	Log::getCoreLogger()->critical(__VA_ARGS__)

// Client log macros
#define CORE_TRACE(...)				Log::getClientLogger()->trace(__VA_ARGS__)
#define CORE_INFO(...)				Log::getClientLogger()->info(__VA_ARGS__)
#define CORE_WARN(...)				Log::getClientLogger()->warn(__VA_ARGS__)
#define CORE_ERROR(...)				Log::getClientLogger()->error(__VA_ARGS__)
#define CORE_CRITICAL(...)			Log::getClientLogger()->critical(__VA_ARGS__)

#pragma warning(pop)

#endif // CORE_LOG_H__