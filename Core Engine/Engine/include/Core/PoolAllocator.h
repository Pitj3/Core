#ifndef CORE_POOLALLOCATOR_H__
#define CORE_POOLALLOCATOR_H__

#include <list>

class PoolAllocator
{
	public:
		PoolAllocator();
		PoolAllocator(const size_t aAlignment, const size_t aNumBlocks, const size_t aSizePerBlock);
		~PoolAllocator();

		void* getBlock();
		void freeBlock(void* aBlock);

	private:
		void* mMem;

		size_t mAlignment;
		size_t mNumBlocksTotal;
		size_t mSizeofBlock;

		std::list<void*> mFreeBlocks;
};

#endif // CORE_POOLALLOCATOR_H__