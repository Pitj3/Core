#ifndef CORE_WINDOW_H__
#define CORE_WINDOW_H__

#include <string>

#include <GLFW/glfw3.h>
#include "Event/Event.h"

struct WindowProps
{
	std::string title;

	uint32_t width;
	uint32_t height;

	explicit WindowProps(const std::string_view& aTitle = "Core Engine", const uint32_t aWidth = 1280, const uint32_t aHeight = 720)
		: title(aTitle), width(aWidth), height(aHeight)
	{
		
	}
};

class Window
{
	public:
		using EventCallbackFunction = std::function<void(Event&)>;
		explicit Window(const WindowProps& aProps = WindowProps());
		~Window();

		void onUpdate() const;

		uint32_t getWidth() const;
		uint32_t getHeight() const;

		void setEventCallback(const EventCallbackFunction& aCallback);

		void setVSync(const bool aEnabled);
		bool isVSync() const;

		GLFWwindow* getNativeWindow() const { return mWindow; }

	private:
		GLFWwindow* mWindow;

		struct WindowData
		{
			std::string title;

			uint32_t width;
			uint32_t height;

			bool vSync;

			EventCallbackFunction eventCallback;
		};

		WindowData mData;
};

#endif // CORE_WINDOW_H__