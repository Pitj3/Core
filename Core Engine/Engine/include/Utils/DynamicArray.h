#ifndef CORE_DYNAMICARRAY_H__
#define CORE_DYNAMICARRAY_H__

#include <vector>

template<typename T>
class DynamicArray
{
	public:
		DynamicArray() = default;
		~DynamicArray() = default;

		DynamicArray& operator = (const std::vector<T>& aOther)
		{
			mData.clear();
			for (auto value : aOther)
			{
				mData.push_back(value);
			}

			return *this;
		}

		DynamicArray& operator = (T* aOther)
		{
			mData.clear();
			//mData.resize(aAmount);
			//for (size_t i = 0; i < aAmount; i++)
		//	{
			//	mData.emplace_back(aOther[i]);
//			}

			return *this;
		}

		T operator [] (size_t aIndex)
		{
			if (aIndex >= mData.size())
			{
				return mData[0];
			}

			return mData[aIndex];
		}

		std::vector<T> get() const
		{
			return mData;
		}

	private:
		std::vector<T> mData;
};

#endif // CORE_DYNAMICARRAY_H__