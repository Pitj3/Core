#ifndef CORE_ASSET_H__
#define CORE_ASSET_H__

#include <string>

class Asset
{
	friend class AssetManager;
	public:
		Asset() = default;
		virtual ~Asset() = default;

		bool isLoaded() const { return mLoaded; }

	protected:
		bool mLoaded = false;
		std::string mName = "Asset";

		virtual void _load() {}
};

#endif // CORE_ASSET_H__
