#ifndef CORE_MESHASSET_H__
#define CORE_MESHASSET_H__

#include <vector>

#include "Assets/Asset.h"
#include "Graphics/StaticMesh.h"
#include "ECS/Entity.h"
#include "Math/Quaternion.h"

struct MeshNode
{
	StaticMesh* mesh;
	Vector3f position;
	Quaternionf rotation;
	Vector3f scale;

	std::vector<MeshNode*> children;
};

class MeshAsset final : public Asset
{
	friend class AssetManager;
	public:
		explicit MeshAsset(const std::string& aPath);
		~MeshAsset();

		StaticMesh* getMesh(const size_t aIndex) const;

		Entity* constructEntityFromAsset();
		
	protected:
		void _load() override;

	private:
		std::string mPath;
		std::vector<StaticMesh*> mMeshes;
		std::vector<MeshNode*> mNodes;
};

#endif // CORE_MESHASSET_H__