#ifndef CORE_VECTOR2_H__
#define CORE_VECTOR2_H__

#include "Math/VectorType.h"

template<typename T>
class Vector2
{
	public:
		Vector2()
		{
			_internal_value = detail::VectorType<T, 2>(0, 0);
		}

		explicit Vector2(const T aValue)
		{
			_internal_value = detail::VectorType<T, 2>(aValue, aValue);
		}

		Vector2(const T aX, const T aY)
		{
			_internal_value = detail::VectorType<T, 2>(aX, aY);
		}

		Vector2(const Vector2& aOther)
		{
			_internal_value = aOther._internal_value;
		}

		Vector2(Vector2&& aOther) noexcept
		{
			_internal_value = std::move(aOther._internal_value);
		}

		bool operator == (const Vector2& aOther)
		{
			return _internal_value == aOther._internal_value;
		}

		bool operator != (const Vector2& aOther)
		{
			return _internal_value != aOther._internal_value;
		}

		Vector2& operator = (const Vector2& aOther)
		{
			_internal_value = aOther._internal_value;
			return *this;
		}

		Vector2& operator = (Vector2&& aOther) noexcept
		{
			_internal_value = std::move(aOther._internal_value);
			return *this;
		}

		Vector2& operator += (const Vector2& aOther)
		{
			_internal_value += aOther._internal_value;
			return *this;
		}

		Vector2& operator -= (const Vector2& aOther)
		{
			_internal_value -= aOther._internal_value;
			return *this;
		}

		Vector2& operator *= (const Vector2& aOther)
		{
			_internal_value = detail::VectorType<T, 2>(x * aOther.x, y * aOther.y);
			return *this;
		}

		Vector2& operator *= (const T aOther)
		{
			_internal_value *= aOther;
			return *this;
		}

		Vector2& operator /= (const Vector2& aOther)
		{
			_internal_value = detail::VectorType<T, 2>(x / aOther.x, y / aOther.y);
			return *this;
		}

		Vector2& operator /= (const T aOther)
		{
			_internal_value /= aOther;
			return *this;
		}

		T distance(const Vector2& aOther) const
		{
			return Diligent::length(aOther._internal_value - _internal_value);
		}

		T dot(const Vector2& aOther) const
		{
			return Diligent::dot(_internal_value, aOther._internal_value);
		}

		T length() const
		{
			return Diligent::length(_internal_value);
		}

		Vector2& normalize()
		{
			_internal_value = Diligent::normalize(_internal_value);
			return *this;
		}

		Vector2 normalized()
		{
			return Diligent::normalize(_internal_value);
		}

		Vector2& lerp(const Vector2& aOther, T aBlend)
		{
			_internal_value = Diligent::lerp(_internal_value, aOther._internal_value, aBlend);
			return *this;
		}

		union
		{
			struct
			{
				T x, y;
			};

			T v[2];

			detail::VectorType<T, 2> _internal_value;
		};

	private:
		Vector2(const detail::VectorType<T, 2> & aValue)
		{
			_internal_value = aValue;
		}
};

template<typename T>
Vector2<T> operator + (Vector2<T> aLeft, const Vector2<T>& aRight)
{
	aLeft += aRight;
	return aLeft;
}

template<typename T>
Vector2<T> operator - (Vector2<T> aLeft, const Vector2<T>& aRight)
{
	aLeft -= aRight;
	return aLeft;
}

template<typename T>
Vector2<T> operator * (Vector2<T> aLeft, const Vector2<T>& aRight)
{
	aLeft *= aRight;
	return aLeft;
}

template<typename T>
Vector2<T> operator * (Vector2<T> aLeft, const T aRight)
{
	aLeft *= aRight;
	return aLeft;
}

template<typename T>
Vector2<T> operator / (Vector2<T> aLeft, const Vector2<T>& aRight)
{
	aLeft /= aRight;
	return aLeft;
}

template<typename T>
Vector2<T> operator / (Vector2<T> aLeft, const T aRight)
{
	aLeft /= aRight;
	return aLeft;
}

using Vector2b = Vector2<bool>;
using Vector2u = Vector2<uint32_t>;
using Vector2i = Vector2<int32_t>;
using Vector2f = Vector2<float>;
using Vector2d = Vector2<double>;

#endif // CORE_VECTOR2_H__