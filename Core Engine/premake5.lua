-- Main Premake
workspace "Core"
    configurations {
        "Debug",
        "Release",
        "Dist"
    }

    architecture "x64"

    sln = solution()
    outputdir = "%{cfg.buildcfg}/%{cfg.system}/%{cfg.architecture}"

    startproject "Sandbox"

    -- Projects
    include "Engine"
    include "Sandbox"

    -- Dependencies
    include "dependencies/GLFW"

    -- Directories
    IncludeDir = {}
    IncludeDir["catch"] = "%{sln.location}/dependencies/catch2/include"
    IncludeDir["GLFW"] = "%{sln.location}/dependencies/GLFW/include"
    IncludeDir["glm"] = "%{sln.location}/dependencies/glm/include"
    IncludeDir["json"] = "%{sln.location}/dependencies/json/include"
    IncludeDir["spdlog"] = "%{sln.location}/dependencies/spdlog/include"
    IncludeDir["stb"] = "%{sln.location}/dependencies/stb/include"
    IncludeDir["diligentspirv"] = "%{sln.location}/dependencies/Diligent/include"
    IncludeDir["diligent"] = "%{sln.location}/dependencies/Diligent/DiligentCore/headers"
    IncludeDir["tbb"] = "%{sln.location}/dependencies/tbb/include"
    IncludeDir["gltf"] = "%{sln.location}/dependencies/fxgltf/include"

    LibDir = {}
    LibDir["diligent"] = "%{sln.location}/dependencies/Diligent/DiligentCore/lib/Debug"
    LibDir["diligentspirv"] = "%{sln.location}/dependencies/Diligent/lib"
    LibDir["tbb"] = "%{sln.location}/dependencies/tbb/lib"

    BinDir = {}
    BinDir["tbb"] = "%{sln.location/dependencies/tbb/bin"

-- Clean Function
newaction {
    trigger     = "clean",
    description = "clean premake generated files",
    execute     = function ()
       print("cleaning...")

       os.rmdir("./bin-int")
       os.rmdir("./bin")

       os.remove("Core.sln")
       os.remove("Makefile")

       os.remove("Engine/Makefile")
       os.remove("Engine/Engine.vcxproj")
       os.remove("Engine/Engine.vcxproj.filters")
       os.remove("Engine/Engine.vcxproj.user")

       os.remove("Sandbox/Makefile")
       os.remove("Sandbox/Tests.vcxproj")
       os.remove("Sandbox/Tests.vcxproj.filters")
       os.remove("Sandbox/Tests.vcxproj.user")

       print("done cleaning.")
    end
 }