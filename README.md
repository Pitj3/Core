![Core Logo](Images/CoreLogoSide_HD_Dark_Resized.png)


| **Platform** | **32 Bit** | **64 Bit** |
|---|---|---|
|**Windows**         | TBA Soon | TBA Soon |
|**Linux**         | TBA Soon | TBA Soon |
|**OSX**         | TBA Soon | TBA Soon |

Website: TBA

Email: roderick.griffioen@gmail.com

#### Why Core Engine?

The Core Engine is a powerful game engine. Written in C++ it delivers great performance and great quality.
The engine is currently in very early baby stage. Check in from time to time to see the engine progress.

#### Core Tech
When the engine is finished it will be/contain:

- Consistent 2D/3D API available in C++ and C#
- High performance engine core written in C++.
- 2D and 3D physics available through Box2D and PhysX.
- High quality sound engine.
- Core Engine Shading Language (CESL) for writing cross platform shaders.

---

#### Core Engine Roadmap
Do you want to know what is planned in the future for the Core Engine?
[Trello Roadmap](https://trello.com/b/OCBe57G3/core-engine-roadmap)


---

Logo is made by Konstantinos Mourelas
